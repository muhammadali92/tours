/*
SQLyog Ultimate v11.33 (32 bit)
MySQL - 10.1.31-MariaDB : Database - tour
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tour` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tour`;

/*Table structure for table `tbl_agent` */

DROP TABLE IF EXISTS `tbl_agent`;

CREATE TABLE `tbl_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) DEFAULT NULL,
  `contact_person` varchar(100) DEFAULT NULL,
  `phone_number` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `company_website` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `company_banner` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_agent` */

insert  into `tbl_agent`(`id`,`company_name`,`contact_person`,`phone_number`,`address`,`city`,`country_id`,`company_website`,`email`,`password`,`logo`,`company_banner`,`status`,`created_date`,`modified_date`,`hash`) values (1,'my company','raheel','6666666666666',NULL,'karachi',0,NULL,'test@gmai.co','05a671c66aefea124cc08b76ea6d30bb',NULL,NULL,1,'2018-06-05 10:38:28','2018-06-05 10:38:28','0e61c8430dd4ed869c9ea0e4b94300c7'),(2,'my company','Nadir ','9666666',NULL,'karachi',0,NULL,'phpsols.developer@gmail.com','05a671c66aefea124cc08b76ea6d30bb',NULL,NULL,1,'2018-06-06 13:01:15','2018-06-06 13:01:15','d0834f89b778fa32b3d1e4e2aa2d9e66');

/*Table structure for table `tbl_clients` */

DROP TABLE IF EXISTS `tbl_clients`;

CREATE TABLE `tbl_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `phone_number` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `countryId` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login_type` enum('fb','google','manual') DEFAULT 'manual',
  `social_id` varchar(100) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_clients` */

insert  into `tbl_clients`(`id`,`name`,`email`,`password`,`phone_number`,`address`,`city`,`countryId`,`status`,`created_date`,`modified_date`,`login_type`,`social_id`,`hash`) values (1,'Muhammad','alii.engineerr@gmail.com','05a671c66aefea124cc08b76ea6d30bb','7896666',NULL,'khi',0,0,'2018-06-05 08:35:08','2018-06-05 08:35:08','manual',NULL,'38eeb70052d0ac43e388fa8259c31f86'),(2,'Muhammad','alii.enginederr@gmail.com','05a671c66aefea124cc08b76ea6d30bb','9666666',NULL,'karachi',0,1,'2018-06-05 09:10:12','2018-06-05 09:10:12','manual',NULL,'6b6132c3bb431fb9182b97b109af70c4'),(3,'Muhammad','ali.engineerr@gmail.com','05a671c66aefea124cc08b76ea6d30bb','9666666',NULL,'karachi',0,1,'2018-06-05 09:11:23','2018-06-05 09:11:23','manual',NULL,'efc976f3614e4ced4494ea7c697a867f'),(4,'khan','test@gmail.com','05a671c66aefea124cc08b76ea6d30bb','666666666',NULL,'karachi',0,1,'2018-06-05 09:12:20','2018-06-05 09:12:20','manual',NULL,'1aedb8d9dc4751e229a335e371db8058'),(5,'ahmed','engineerr@gmail.com','05a671c66aefea124cc08b76ea6d30bb','666666666666',NULL,'khi',0,1,'2018-06-05 09:14:18','2018-06-05 09:14:18','manual',NULL,'823388afe91cb0e9ad2f01aebd110cd2'),(6,'raheem','khan@gmail.com','05a671c66aefea124cc08b76ea6d30bb','8966666',NULL,'khi',0,1,'2018-06-05 09:15:46','2018-06-05 09:15:46','manual',NULL,'37acdff68ce3d4d516b3f64ffcc4b067');

/*Table structure for table `tbl_flights` */

DROP TABLE IF EXISTS `tbl_flights`;

CREATE TABLE `tbl_flights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flight_name` varchar(255) DEFAULT NULL,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `from_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `to_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `package_id` int(1) DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `class` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_flights` */

/*Table structure for table `tbl_package` */

DROP TABLE IF EXISTS `tbl_package`;

CREATE TABLE `tbl_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_title` varchar(255) DEFAULT NULL,
  `package_desc` text,
  `package_days` varchar(255) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL,
  `start_from` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `views` varchar(100) DEFAULT '0',
  `status` int(1) DEFAULT '0',
  `vendor_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `off` varchar(100) DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `terms_and_conditions` text,
  `discount_price` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_package` */

insert  into `tbl_package`(`id`,`package_title`,`package_desc`,`package_days`,`price`,`start_from`,`views`,`status`,`vendor_id`,`created_date`,`off`,`end_date`,`terms_and_conditions`,`discount_price`) values (1,'PACKAGE','PACKAGE PACKAGE PACKAGE','6 days 5 night','600','2018-06-09 00:00:00',NULL,0,2,'2018-06-26 08:49:51',NULL,'2018-06-30 00:00:00','das sad asd sad sad sadsadasdsa d asdasd','500');

/*Table structure for table `tbl_packages_img` */

DROP TABLE IF EXISTS `tbl_packages_img`;

CREATE TABLE `tbl_packages_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_packages_img` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
