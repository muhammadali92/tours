<!doctype html>
<html>
    <head>
    <meta charset="UTF-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->

    <!-- CSS Reset -->
    <style type="text/css">

html,  body {
  margin: 0 !important;
  padding: 0 !important;
  height: 100% !important;
  width: 100% !important;
}
    {
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}
.ExternalClass {
  width: 100%;
}
div[style*="margin: 16px 0"] {
  margin: 0 !important;
}
table,  td {
  mso-table-lspace: 0pt !important;
  mso-table-rspace: 0pt !important;
}

table {
  border-spacing: 0 !important;
  border-collapse: collapse !important;
  table-layout: fixed !important;
  margin: 0 auto !important;
}
table table table {
  table-layout: auto;
}
img {
  -ms-interpolation-mode: bicubic;
}
.yshortcuts a {
  border-bottom: none !important;
}
a[x-apple-data-detectors] {
  color: inherit !important;
}
</style>

<!-- Progressive Enhancements -->
<style type="text/css">
    
    /* What it does: Hover styles for buttons */
    .button-td,
    .button-a {
        transition: all 100ms ease-in;
    }
    .button-td:hover,
    .button-a:hover {
        background: #555555 !important;
        border-color: #555555 !important;
    }

    /* Media Queries */
    @media screen and (max-width: 600px) {

        .email-container {
            width: 100% !important;
        }

        /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
        .fluid,
        .fluid-centered {
            max-width: 100% !important;
            height: auto !important;
            margin-left: auto !important;
            margin-right: auto !important;
        }
        /* And center justify these ones. */
        .fluid-centered {
            margin-left: auto !important;
            margin-right: auto !important;
        }

        /* What it does: Forces table cells into full-width rows. */
        .stack-column,
        .stack-column-center {
            display: block !important;
            width: 100% !important;
            max-width: 100% !important;
            direction: ltr !important;
        }
        /* And center justify these ones. */
        .stack-column-center {
            text-align: center !important;
        }
    
        /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
        .center-on-narrow {
            text-align: center !important;
            display: block !important;
            margin-left: auto !important;
            margin-right: auto !important;
            float: none !important;
        }
        table.center-on-narrow {
            display: inline-block !important;
        }
            
    }

</style>
    </head>
    <body bgcolor="#e0e0e0" width="100%" style="margin: 0;">
    
    <table bgcolor="#e0e0e0" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
      <tr>
        <td>
           <center style="width: 100%;">
            <br><br>
            <!-- Email Body : BEGIN -->
            <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="600" class="email-container" style="border: solid 1px  #C2C2C2;">
            <tr>
              <!-- Email Header : BEGIN -->
                <td bgcolor="#cecece" style="padding: 20px 0 10px; text-align: center"><h2> Tours </h2> </td>
              <!-- Email Header : END --> 
            </tr>
            <!-- Hero Image, Flush : BEGIN -->
            <tr>
                <td class="full-width-image"></td>
              </tr>
            <!-- Hero Image, Flush : END -->

<!-- Email Body : BEGIN -->
<!--
<tr> 
  <td align="center" style="font-size: 32px; font-weight: 300; line-height: 3.5em; color: #929292; font-family: sans-serif;">Consultant Invitation</td>
  </tr>
  -->
  <tr> 
  <!-- row container for User -->
  <td align="center" style="font-size: 18px; font-weight:500; line-height: 2.5em; color: black; font-family: sans-serif;">Hi <?php echo $name ?>,</td>
  </tr>
  <tr> 
  <!-- row container for Tagline -->
  <td align="center" style="font-size: 16px; font-weight:500; color: #929292; font-family: sans-serif;">
        <p  style="margin-right:20px;font-size: 17px; font-weight:500; color: #222222; font-family: sans-serif;" >
            We have successfully created your account at Tours. <br/>  </p>
<p style="color:black" >
        You can begin using your account after verifying your email. <br/>
            Your email  verification link is below  </p>


      
  </td>
  </tr>
      
      <!-- 1 Column Text : BEGIN -->
      <tr>
          <td style="padding: 10px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;"> 
          
          <!-- Button : Begin -->

<a href="<?php echo base_url().'user-verification/'.$hash; ?>" >
            <button style="background-color: #008ED6; 
            border: none;
            color: white;
            padding: 10px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;" > Verify Here</button>
            </a>          
            <p style="color:black" >
<br/><br/>            
        <b>Tour Team </b> </p>


          <!-- Button : END --></td>
          <p>&nbsp; </p>


        </tr>
    
    
       
    </table>

<!-- Email Body : END --> 


<!-- Email Footer : BEGIN -->
            <table align="center" width="600" class="email-container">
            <tr>
                <td background="" bgcolor="" valign="middle" style="text-align: center; background-position: center center !important; background-size: cover !important;"><!--[if gte mso 9]>
                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;height:175px; background-position: center center !important;">
                    <v:fill type="tile" src="" color="#222222" />
                    <v:textbox inset="0,0,0,0">
                    <![endif]-->
                
                <div>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
</td>
                      
            </tr>
                  </table>
                  </div>
                
                <!--[if gte mso 9]>
                    </v:textbox>
                    </v:rect>
                    <![endif]--></td>
              </tr>
            
      
        
          </table>
            <!-- Email Footer : END -->
            
          </center></td>
      </tr>
    </table>
</body>
</html>
