
<!DOCTYPE html>
<html>
<head>
	<title>Client's Panel</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/matiera/bootstrap.min.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/simple-line-icons/css/simple-line-icons.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/style-client.css">
</head>
<body>
	<div class="topbar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-2">
					
				</div>
				<div class="col-sm-5">
					<h4>Welcome Muhammad Ahsan</h4> 
					
				</div>
				<div class="col-sm-3">
					<h4>Agent Portal</h4> 
				</div>
				<div class="col-sm-2 topbar-btn-cont">
					<div class="row">
						<div class="col-xs-6">


					 	<a href="messages.php" class="btn btn-default btn-lg btn-link" style="font-size:22px;color: #fff;">
					 		<span class="icon icon-speech"></span>
					 	</a>
					    	<span class="badge badge-notify">3</span>
					    </div>
						<div class="col-xs-6">
							<div class="dropdown">						
								<button class="btn btn-default btn-lg btn-link dropdown-toggle" data-toggle="dropdown" style="font-size:22px;color: #fff;font-weight: 300;">
									<span class="icon icon-user"></span>
							    </button>
							    <div class="dropdown-menu">
				    				<a class="dropdown-item" href="#"><span class="icon icon-settings"></span> Settings</a>
				    				<a class="dropdown-item" href="#"><span class="icon icon-logout"></span> Logout</a>
				  				</div>
			  				</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid no-gutters" style="height: 100%;">
		<div class="row">
			<div class="col-sm-2 no-padding">
				<nav class="navbar bg-light">
				  <ul class="navbar-nav">
				    <li class="nav-item">
				      <a class="nav-link" href="dashboard.php"><span class="icon icon-list"></span> News Feed</a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" href="messages.php"><span class="icon icon-speech"></span> Package Request <span class="badge badge-danger">3</span></a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" href="messages.php"><span class="icon icon-speech"></span> Messages <span class="badge badge-danger">6</span></a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" href="#"><span class="icon icon-share"></span> Package Builder</a>
				    </li>
				  </ul>
				</nav>
			</div>
		