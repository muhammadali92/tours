<?php 
	include 'header.php';
?>

	<div class="col-sm-10 content-div">
		<div class="row">
			<div class="col-sm-12">
				<h2>Messages</h2>
			</div>
		</div>		  
		<div class="row">
			<div class="col-sm-3">
				<div class="card message-card">
				    <div class="card-header">
				    	<div class="row">
					  		<div class="input-group">							
								<input type="text" id="searchbar" class="form-control" name="search" placeholder="Search meassages...">
								<span class="input-group-addon"><button type="button" class="btn btn-success"><span class="icon icon-magnifier"></span></button></span>
							</div>
					  	</div>
					</div>
				    <div class="card-cont">
				  		<div class="card-body messages-list">
						  	<ul id="user-list" class="nav nav-tabs flex-column" role="tablist">
							    <li class="nav-item">
							      <a class="nav-link active" data-toggle="tab" href="#message1"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/admin.jpg);"></div>Admin</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message2"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/rdj.png);"></div>Muhammad Ali</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message3"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/rock.jpg);"></div>Muhammad Ahsan Ali</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message4"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-male.png);"></div>Unknown User 1</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message5"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-female.png);"></div>Unknown User 2</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message5"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-female.png);"></div>Unknown User 2</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message5"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-female.png);"></div>Unknown User 2</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message5"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-female.png);"></div>Unknown User 2</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message5"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-female.png);"></div>Unknown User 2</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message5"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-female.png);"></div>Unknown User 2</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message5"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-female.png);"></div>Unknown User 2</a>
							    </li>
							    <li class="nav-item">
							      <a class="nav-link" data-toggle="tab" href="#message5"><div class="user-image-thumb rounded-circle" style="background-image: url(../assets/img/website-icons/user-female.png);"></div>Unknown User 2</a>
							    </li>
							</ul>
						</div>					  
				    </div>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="card message-card">
					<div class="card-header">Messages</div>
						<div class="card-body">
							<div class="tab-content">
								<div id="message1" class="container tab-pane active">
									<div class="row">
										<div class="col-sm-10">
											From: Admin
										</div>
										<div class="col-sm-2">
											<a href="#" class="btn"><span class="icon icon-trash text-danger"></span></a>
										</div>
									</div>	
									<div class="row message-cont">
										<div class="col-sm-8 msg-bubble">
											<p>All the time I thought about you	I saw your eyes and they were so blue I could read there just one name My name, my name, my name</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-8 msg-bubble">
											<p>Because of you I'm flying higher	You give me love, you set a fire You keep me warm when you call my name.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-8 offset-sm-4 msg-bubble msg-bubble-me">
											<p>That's my name, that's my name, that's my name.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-10 no-padding">
											<textarea rows="4" placeholder="Write your message here..."></textarea>
										</div>
										<div class="col-sm-2">
											<button type="submit" class="btn btn-success">Send</button>
										</div>
									</div>
								</div>

								<div id="message2" class="container tab-pane fade">
									<div class="row">
										<div class="col-sm-10">
											From: Muhammad Ali
										</div>
										<div class="col-sm-2">
											<a href="#" class="btn"><span class="icon icon-trash text-danger"></span></a>
										</div>
									</div>	
									<div class="row message-cont">
										<div class="col-sm-8 msg-bubble">
											<p>Message 2</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-8 msg-bubble">
											<p>Hi my name is Muhammad Ahsan Ali. I am a Web Developer.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-10 no-padding">
											<textarea rows="4" placeholder="Write your message here..."></textarea>
										</div>
										<div class="col-sm-2">
											<button type="submit" class="btn btn-success">Send</button>
										</div>
									</div>
								</div>

								<div id="message3" class="container tab-pane fade">
									<div class="row">
										<div class="col-sm-10">
											From: Muhammad Ahsan Ali
										</div>
										<div class="col-sm-2">
											<a href="#" class="btn"><span class="icon icon-trash text-danger"></span></a>
										</div>
									</div>	
									<div class="row message-cont">
										<div class="col-sm-8 msg-bubble">
											<p>All the time I thought about you	I saw your eyes and they were so blue I could read there just one name My name, my name, my name</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-8 msg-bubble">
											<p>Because of you I'm flying higher	You give me love, you set a fire You keep me warm when you call my name.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-8 offset-sm-4 msg-bubble msg-bubble-me">
											<p>That's my name, that's my name, that's my name.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-10 no-padding">
											<textarea rows="4" placeholder="Write your message here..."></textarea>
										</div>
										<div class="col-sm-2">
											<button type="submit" class="btn btn-success">Send</button>
										</div>
									</div>
								</div>

								<div id="message4" class="container tab-pane fade">
									<div class="row">
										<div class="col-sm-10">
											From: Unknown User 1
										</div>
										<div class="col-sm-2">
											<a href="#" class="btn"><span class="icon icon-trash text-danger"></span></a>
										</div>
									</div>	
									<div class="row message-cont">
										<div class="col-sm-8 msg-bubble">
											<p>This message is from unknown user 1. I don't have my picture to upload.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-8 offset-sm-4 msg-bubble msg-bubble-me">
											<p>Okay.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>

										<div class="col-sm-10 no-padding">
											<textarea rows="4" placeholder="Write your message here..."></textarea>
										</div>
										<div class="col-sm-2">
											<button type="submit" class="btn btn-success">Send</button>
										</div>
									</div>
								</div>

								<div id="message5" class="container tab-pane fade">
									<div class="row">
										<div class="col-sm-10">
											From: Unknown User 2
										</div>
										<div class="col-sm-2">
											<a href="#" class="btn"><span class="icon icon-trash text-danger"></span></a>
										</div>
									</div>	
									<div class="row message-cont">
										<div class="col-sm-8 msg-bubble">
											<p>This message is from unknown user 1. I don't have my picture to upload.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-8 offset-sm-4 msg-bubble msg-bubble-me">
											<p>Okay.</p>
											<span class="date"><?php echo date('Y-m-d h:i:sa');?></span>
										</div>
										<div class="col-sm-10 no-padding">
											<textarea rows="4" placeholder="Write your message here..."></textarea>
										</div>
										<div class="col-sm-2">
											<button type="submit" class="btn btn-success">Send</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
<?php include 'footer.php';?>