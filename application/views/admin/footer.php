		</div>
	</div>
	<!-- jQuery library -->
	<script src="../assets/js/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="../assets/js/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="../assets/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../assets/js/jquery.ticker.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
	<script type="text/javascript">
		var ctx = document.getElementById('myChart').getContext('2d');
		var chart = new Chart(ctx, {
		    // The type of chart we want to create
		    type: 'line',
		    responsive:true,
		    defaultFontColor:'#fff',
			defaultFontFamily:"'Montserrat',sans-serif",
		    // The data for our dataset
		    data: {
		        labels: ["January", "February", "March", "April", "May", "June", "July"],
		        datasets: [{
		            label: "My First dataset",
		            backgroundColor: '#fff',
		            borderColor: '#fff',
		            data: [0, 10, 5, 2, 20, 30, 45],
		        }]
		    },

		    // Configuration options go here
		    options: {}
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.ticker').show();
			$('.ticker').ticker({
				fade:true,
				cursorOne:'',  
			    cursorTwo:'',
			    cursorSpeed:   00,
			});
		});
		

		// $.fn.ticker.defaults = {
		//   random:        false, // Whether to display ticker items in a random order
		//   itemSpeed:     3000,  // The pause on each ticker item before being replaced
		//   cursorSpeed:   50,    // Speed at which the characters are typed
		//   pauseOnHover:  true,  // Whether to pause when the mouse hovers over the ticker
		//   finishOnHover: true,  // Whether or not to complete the ticker item instantly when moused over
		//   cursorOne:     '',   // The symbol for the first part of the cursor
		//   cursorTwo:     '',   // The symbol for the second part of the cursor
		//   fade:          false,  // Whether to fade between ticker items or not
		//   fadeInSpeed:   600,   // Speed of the fade-in animation
		//   fadeOutSpeed:  300    // Speed of the fade-out animation
		// };
	</script>	
	<script type="text/javascript">
		$(document).ready(function(){
		  $("#searchbar").on("keyup", function() {
		    var value = $(this).val().toLowerCase();
		    $("#user-list li").filter(function() {
		      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		    });
		  });
		});
	</script>
</body>
</html>