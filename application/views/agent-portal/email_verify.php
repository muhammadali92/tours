<?php $this->load->view('agent-portal/header');?>	
			<div class="col-sm-9">
				<div class="jumbotron">
				  <h1 class="text-whitet"><i class="icon icon-envelope"></i> Email Verify</h1> 
				  <p>Time to check your email. We have sent you a verification code.<br>Please submit the verification code below.</p> 
				</div>
				<form action="#">
					<div class="row">
						<div class="col-sm-6">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Verifcation Box</div>
								<div class="card-body">
									<div class="form-group">
										<label class="col-form-label" for="verification-code">Verification Code</label>
										<input type="password" class="form-control password" placeholder="Enter verification code" id="verification-code" name="verification-code">
									</div>		
									<button type="submit" class="btn btn-outline-primary btn-sm">Resend Verification Code</button>												
							    </div>
							</div>

							<button type="submit" class="btn btn-primary">Verify</button>

						</div>	

							

					</div>
					
				</form>
			</div>

<?php $this->load->view('agent-portal/footer');?>
