<?php $this->load->view('agent-portal/header');?>	
			<div class="col-sm-9">
				<div class="jumbotron">
				  <h1>Package Builder</h1> 
				  <p>Build your own package.</p> 
				</div>
				<form action="#">
					<div class="row">
						<div class="col-sm-6">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Mandatory Information</div>
								<div class="card-body">
									<div class="form-group">
										<label class="col-form-label" for="package-title">Package Title</label>
										<input type="text" class="form-control" placeholder="Enter Package Title" id="package-title" name="package-title">
									</div>
									<div class="form-group">
										<label class="control-label">Duration</label>
										<div class="form-group">
											<div class="input-group mb-3">
												<input type="text" class="form-control" placeholder="Days" name="days">
												<input type="text" class="form-control" placeholder="Nights" name="nights">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="total-travelers">Travlers</label>
										<input type="text" class="form-control" placeholder="Enter total travelers" id="total-travelers" name="total-travelers">
									</div>
							    </div>
							</div>
						</div>	

						<div class="col-sm-6">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Flight Information</div>
								<div class="card-body">

									<div class="form-group">
										<label class="control-label">Departure</label>
										<div class="form-group">
											<div class="input-group mb-3">
												<input type="text" class="form-control" placeholder="Departure location" name="departure-location">
												<input type="text" class="form-control" placeholder="Departure date" name="departure-date">
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Return location" name="return-location">
											<input type="text" class="form-control" placeholder="Return date" name="return-date">
										</div>
									</div>

									<div class="form-group">
										<label for="flight-class">Class:</label>
										<select class="form-control" id="flight-class">
											<option>Business</option>
											<option>Economy</option>
										</select>
									</div>
							    </div>
							</div>
						</div>	

						<div class="col-sm-12">	
							<div class="card bg-secondary mb-3">
								<div class="card-header">Itineraries <button type="button" class="btn btn-outline-primary btn-sm float-right" id="addNewItinerary">Add New Itinerary</button></div>
								<div class="card-body itineraryContainer" id="itineraryCard">




									<!-- <div class="card border-dark mb-3">
										<div class="card-header">
											<div class="row">
												<div class="col-sm-2">
													<span class="badge badge-primary">0 Day(s)</span>
												</div>
												<div class="col-sm-4">
													<input type="text" class="form-control bg-primary text-white" placeholder="Enter Title" name="days">	
												</div>
												<div class="col-sm-4">
													<input type="text" class="form-control bg-primary text-white" placeholder="Enter Arrival" name="days">	
												</div>
												<div class="col-sm-2">
													<button type="button" class="btn btn-outline-primary btn-sm float-right" id="addNewItinerary">Add Day</button>
												</div>	

											</div>
										</div>

										<div class="card-body itineraryDaysContainer">
											<div class="alert alert-dismissible alert-primary">
												<button type="button" class="close" data-dismiss="alert">&times;</button>
												<p class="mb-0">Day 1</p>
												<div class="row">
													<div class="col-sm-4">									
														<input type="text" class="form-control" placeholder="Title" name="days">	
													</div>
													<div class="col-sm-8">									
														<textarea class="form-control" placeholder="Description"></textarea>
													</div>
												</div>
											</div>		
									    </div>
									</div> -->



							    </div>
							</div>						
						</div>

						<div class="col-sm-8">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Terms & Conditions</div>
								<div class="card-body">
									<div class="form-group">
										<label for="terms-conditions">Terms & Conditions</label>
										<textarea rows="10" class="form-control" placeholder="Enter your terms & conditions."></textarea>
									</div>
								</div>
							</div>							
						</div>


						<div class="col-sm-4">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Local Transfers / Transportation </div>
								<div class="card-body">
									<div class="form-group">
										<label for="">Local Transfers</label>
										<textarea rows="10" class="form-control" placeholder="Enter all local transfers included in this package (Use * for bullet points)."></textarea>
									</div>
								</div>
							</div>
						</div>


						<div class="col-sm-4">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Pricing</div>
								<div class="card-body">
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text">$</span>
											</div>
											<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
											<div class="input-group-append">
												<span class="input-group-text">.00</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>



					</div>
					
				</form>
			</div>
<?php $this->load->view('agent-portal/footer');?>
