<section class="no-padding account-status">
	<p><i class="icon icon-info"></i> Please complete your account information from <a href="<?php echo base_url();?>agent/account_settings">account settings</a> to activate your account. </p>
</section>