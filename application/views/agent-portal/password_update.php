<?php $this->load->view('agent-portal/header');?>	
			<div class="col-sm-9">
				<div class="jumbotron">
				  <h1 class="text-whitet"><i class="icon icon-lock"></i> Password Update</h1> 
				  <p>Security is password. Make sure to choose stonger security.</p> 
				</div>
				<form action="#">
					<div class="row">
						<div class="col-sm-6">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Password Information</div>
								<div class="card-body">
									<div class="form-group">
										<label class="col-form-label" for="old-password">Old Password</label>
										<input type="password" class="form-control password" placeholder="Old Password" id="old-password" name="old-password" value="oldpassword">
									</div>

									<div class="form-group">
										<label class="col-form-label" for="new-password">New Password</label>
										<input type="password" class="form-control password" placeholder="Old Password" id="new-password" name="new-password" value="newpassword">
									</div>

									<div class="form-group">
										<label class="col-form-label" for="new-password2">New Password Confirm</label>
										<input type="password" class="form-control password" placeholder="Old Password" id="new-password2" name="new-password2" value="newpassword">
									</div>

									<button type="button" class="btn btn-sm btn-outline-primary" id="showPassword">Show Password</button>

									<button type="button" class="btn btn-sm btn-outline-primary" id="hidePassword">Hide Password</button>

									
									
							    </div>
							</div>
							<button type="submit" class="btn btn-primary">Update</button>

						</div>	

							

					</div>
					
				</form>
			</div>

<?php $this->load->view('agent-portal/footer');?>
