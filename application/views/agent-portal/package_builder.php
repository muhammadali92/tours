<?php $this->load->view('agent-portal/header');?>	
<style>
.pull-right {
        float: none !important;
    }
</style>
			<div class="col-sm-9">
				<div class="jumbotron">
				  <h1>Create Package</h1> 
				</div>
				<form  id="package-form" action="#">
					<div class="row">
						<div class="col-sm-12">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Basic Information</div>
								<div class="card-body">
									<div class="form-group">
										<label class="col-form-label" for="package-title">Category</label>
										<select  id="category" class="form-control">
										<option value="0" >Select Category</option>
										<option value="1" >Deals</option>
										</select>
									</div>

								<div class="form-group">
										<label class="col-form-label" for="package-title">Package Title</label>
										<input type="text" class="form-control" placeholder="Enter Package Title" id="packageTitle" name="package-title">
									</div>
									<div class="form-group">
										<label class="col-form-label" for="package-title">Package Desc</label>
										<textarea class="form-control" cols="8" rows="10" placeholder="Package Desc Here" id="packageDesc" name="package-title"></textarea>
									</div>
									
									<div class="form-group">
										<label class="control-label">Duration</label>
										<div class="form-group mb-3">
											<div class="input-group ">
												<input type="number" class="form-control" onkeypress="return isNumber(event)" id="days" placeholder="Days" name="days">
												<input type="number" class="form-control" onkeypress="return isNumber(event)" id="nights" placeholder="Nights" name="days">

											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label">Start and End Date</label>
										<div class="form-group">
											<div class="input-group mb-3">
												<input type="text" class="form-control   datepicker"  readonly="true" id="startDate" placeholder="Start from" name="days">
												<input type="text" class="form-control datepicker" readonly="true" id="endDate" placeholder="End Date" name="nights">
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label">Price</label>
										<div class="form-group">
											<div class="input-group mb-3">
												<input type="number" class="form-control" id="price"  onkeypress="return isNumber(event)" placeholder="Package Price" name="days">
												<input type="number" class="form-control" id="discountPrice"  onkeypress="return isNumber(event)" placeholder="Discount Price" name="nights">
											</div>
										</div>
									</div>
								    </div>
							</div>
						</div>	
						<div class="col-sm-12" >
						<div class="card-header">Upload Images</div>
              <div class="col-sm-6 b-r b-dashed b-grey">
                <div class="padding-0">
                  <br>
                  
                  <div class="bg-master-light padding-10 ">
                    <div class="panel panel-default no-padding no-margin">
                      <div class="panel-heading">
                        <div class="panel-title"></div>
                        <div class="tools"> <a class="collapse" href="javascript:;"></a> <a class="config" data-toggle="modal" href="#grid-config"></a> <a class="reload" href="javascript:;"></a> <a class="remove" href="javascript:;"></a> </div>
                      </div>
                      <div class="panel-body no-scroll no-padding">
                        <form action="<?php echo base_url(); ?>upload-feeds" class="aq-dropzone no-margin">
                          <div id="imagePreview"> </div>
                        </form>
                      </div>
                    </div>
                    <div class="" id="mulitplefileuploader">
					<button  onclick="uploadButton();" class="btn btn-primary btn-block text-lft m-b-0 m-t-10 zero-bordr-radis">
					<i class="fa fa-plus pull-left p-t-5 p-r-5"></i> UPLOAD IMAGES </button></div>
                    <div>
					</div>
                    

                  </div>
                </div>
                <br>
                <p class="small alert alert-warning lupld-ftypes"><span>For faster uploads, upload images less than 4 mb.</span>Files must be: .doc .docx .xls .xlsx .pdf .jpeg .jpg or .png </p>
                
                
                
                
              </div>
						
						</div>
						<div class="col-sm-12">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Flight Information</div>
								<br/>
								<button type="button"  onclick="addMore()" style="margin-left: 830px" class="col-sm-1  btn btn-sm btn-success">Add more</button>
								<div class="card-body">
								<div id="main-flight" >
								 <div class="form-group">
										<label class="control-label">Title</label>
										<div class="form-group">
											<div class="input-group ">
												<input type="text" id="flightTitle0" class="form-control" placeholder="Flight title" name="departure-location">
											</div>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label">Departure</label>
										<div class="form-group">
											<div class="input-group mb-3">
												<input type="text" id="fightDepLoc0" class="form-control " placeholder="Departure location" name="departure-location">
												<input type="text" readonly="true" id="fightStartTime0" class="form-control datepicker" placeholder="Departure date" name="departure-date">
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="input-group mb-3">
											<input type="text" id="flightReturnloc0" class="form-control" placeholder="Return location" name="return-location">
											<input type="text"  readonly="true" id="fightEndTime0" class="form-control datepicker" placeholder="Return date" name="return-date">
										</div>
									</div>

									<div class="form-group">
										<label for="flight-class">Class:</label>
										<select class="form-control" id="flightClass0">
											<option>Business</option>
											<option>Economy</option>
										</select>
									</div>


							    </div>
								</div>
								
							</div>
						</div>	

			<!-- Add Itinerary start -->
			<div class="col-sm-12">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Add Itinerary</div>
								<br/>
								<button type="button"  onclick="addMoreIn()" style="margin-left: 830px" class="col-sm-1  btn btn-sm btn-success">Add more</button>
								<div class="card-body">
								<div id="main-int" >
								 <div class="form-group">
										<label class="control-label">Day</label>
										<div class="form-group">
											<div class="input-group ">
												<input type="text" id="intDay0" class="form-control" placeholder="Day 1" name="sDay">
											</div>
										</div>
								 </div>
								 <div class="form-group">
										<label class="control-label">Location </label>
										<div class="form-group">
											<div class="input-group ">
												<input type="text" id="intTitle0" class="form-control" placeholder="Example:- Arrival in Dubai: Burj Khalifa At The Top" name="sDay">
											</div>
										</div>
								 </div>
								 <div class="form-group">
										<label for="terms-conditions">Details</label>
										<textarea rows="10" class="form-control" id="intDesc0" placeholder="Example:- Dubai is a cascade of beautiful traditions"></textarea>
									</div>





							    </div>
								</div>
								
							</div>
						</div>	
			
			<!-- Add Itinerary end -->

						<div class="col-sm-10">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Terms & Conditions</div>
								<div class="card-body">
									<div class="form-group">
										<label for="terms-conditions">Terms & Conditions</label>
										<textarea rows="10" class="form-control" id="termsCon" placeholder="Enter your terms & conditions."></textarea>
									</div>
								</div>
							</div>							
						</div>




						<div class="col-sm-4">
						<div style="color:red" class="hide errorDiv "></div>
						<br/>
							<button type="button"  onclick="addPackages()" class="btn btn-primary">Submit</button>
						</div>



					</div>
					
				</form>
			</div>
<?php $this->load->view('agent-portal/footer');?>
	<script src="<?php echo base_url(); ?>/assets/js/jquery.fileuploadmulti.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
		var tmp = 0;
		var t = 0;
	    var flightTitle = [];
		var fightStartTime = [];
		var fightDepLoc = [];
		var fightEndTime = [];
		var flightClass = [];
		var flightReturnloc = [];
		var t1 = 0;
		
		var intDay = [];
		var intTitle = [];
		var intDesc = [];

		
 function addMoreIn(){
	 t1++;
	 let html = `<div id="in-${t1}" > <div class="form-group">
				 <label class="control-label">Day</label>
				 <button  style="margin-left:800px"  onclick="removeIn(${t1})" class="btn pull-right btn-sm btn-danger" >remove</button>
				 <div class="form-group">
				 <div class="input-group ">
				 <input type="text" id="intDay${t1}" class="form-control" placeholder="Day 1" name="sDay">
				 </div>
				 </div>
				 </div>
				 <div class="form-group">
				 <label class="control-label">Location </label>
				 <div class="form-group">
				 <div class="input-group ">
				 <input type="text" id="intTitle${t1}" class="form-control" placeholder="Example:- Arrival in Dubai: Burj Khalifa At The Top" name="sDay">
				 </div>
				 </div>
				 </div>
				 <div class="form-group">
				 <label for="terms-conditions">Details</label>
				 <textarea rows="10" class="form-control" id="intDesc${t1}" placeholder="Example:- Dubai is a cascade of beautiful traditions"></textarea>
				 </div>
				 <br/>
				 <hr>
				 <br/>
				 </div>
				 `;
				 $('#main-int').prepend(html);
				 
 }
 function addMore(){
	 $( ".datepicker" ).datepicker();

	  t++;
 let html =  `  <div id="fl-${t}" ><div   class="form-group">
			    <label class="control-label">Title</label>
				<button  style="margin-left:800px"  onclick="removeFl(${t})" class="btn pull-right btn-sm btn-danger" >remove</button>
				<div class="form-group">
				<div class="input-group ">
				<input type="text" class="form-control" id="flightTitle${t}" placeholder="Flight title" name="departure-location">
				</div>
				</div>
				</div>
				<div class="form-group">
				<label class="control-label">Departure</label>
				<div class="form-group">
				<div class="input-group mb-3">
				<input type="text" class="form-control "  id="fightDepLoc${t}" placeholder="Departure location" name="departure-location">
				<input type="text" readonly="true" class="form-control datepicker" id="fightStartTime${t}" placeholder="Departure date" name="departure-date">
				</div>
				</div>
				</div>
				<div class="form-group">
				<div class="input-group mb-3">
				<input type="text" class="form-control" id="flightReturnloc${t}" placeholder="Return location" name="return-location">
				<input type="text" readonly="true" class="form-control datepicker" id="fightEndTime${t}" placeholder="Return date" name="return-date">
				</div>
				</div>
				<div class="form-group">
				<label for="flight-class">Class:</label>
				<select class="form-control" id="flightClass${t}">
				<option>Business</option>
				<option>Economy</option>
				</select>
				</div>
				 </div>
				 <br/>
				 <hr>
				 <br/>
				 </div>
				 `;
				 $( ".datepicker" ).datepicker();
				 $('#main-flight').prepend(html);
				
 }
 
  function getFlightData(){
	     flightTitle = [];
		 fightStartTime = [];
		 fightDepLoc = [];
		 fightEndTime = [];
		 flightClass = [];
		 flightReturnloc = [];
	  
	  for(let i = 0; i <= t;i++){
			console.log(i);
		  if(typeof $('#flightTitle'+i).val() !== 'undefined'){
			flightTitle.push($('#flightTitle'+i).val());
			fightStartTime.push($('#fightStartTime'+i).val());
			fightDepLoc.push($('#fightDepLoc'+i).val());
			fightEndTime.push($('#fightEndTime'+i).val());
			flightClass.push($('#flightClass'+i).val());
			flightReturnloc.push($('#flightReturnloc'+i).val());
				
		  }
	  }
  }
  
  function getIntData(){
	    intDay = [];
		intTitle = [];
		intDesc = [];
	  
	  for(let i = 0; i <= t1;i++){
			console.log(i);
		  if(typeof $('#intDay'+i).val() !== 'undefined'){
			intDay.push($('#intDay'+i).val());
			intTitle.push($('#intTitle'+i).val());
			intDesc.push($('#intDesc'+i).val());
				
		  }
	  }
  }
  
  function addPackages(){
	  
	let category = $('#category').val();
	let packageTitle = $('#packageTitle').val();
	let packageDesc = $('#packageDesc').val();
	let days = $('#days').val();
	let nights = $('#nights').val();

	let startDate = $('#startDate').val();
	let endDate = $('#endDate').val();
	let price = $('#price').val();
	let discountPrice = $('#discountPrice').val();
	let termsCon = $('#termsCon').val();

	
	
	$('#category').css('border-color','gray');
	$('#packageTitle').css('border-color','gray');
	$('#packageDesc').css('border-color','gray');
	$('#days').css('border-color','gray');
	$('#nights').css('border-color','gray');
	$('#startDate').css('border-color','gray');
	$('#endDate').css('border-color','gray');
	$('#price').css('border-color','gray');
	$('#discountPrice').css('border-color','gray');
	getFlightData();
	getIntData();
	
	if(category == 0)
	  {
			$('#category').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please Select Category.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
	  }
	  else if($.trim(packageTitle).length == 0)
	  {
			$('#packageTitle').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Package title required.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
	  }		
	  else if($.trim(packageDesc).length == 0)
		{
			$('#packageDesc').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Package Desc required.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(days).length == 0)
		{
			$('#days').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Days required.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(nights).length == 0)
		{
			$('#nights').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Nights required.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		
		else if($.trim(startDate).length == 0)
		{
			$('#startDate').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please select Start Date.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(endDate).length == 0)
		{
			$('#endDate').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please select end Date.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(price).length == 0)
		{
			$('#price').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Price required.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(discountPrice).length == 0)
		{
			$('#discountPrice').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Discount Price required.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}
		else if(imageName.length === 0)
		{
			$('.errorDiv').html(' <strong>Error!</strong> Please upload atleast 1 image.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
			
		}
		else
		{

			let dayAndNight = `${days} days and ${nights} nights`;
			$.ajax({
				url: '<?php echo base_url(); ?>Api/addPackage',
				type: 'POST',
				dataType: 'HTML',
				data : "packageTitle="+packageTitle+"&packageDesc="+packageDesc+"&dayAndNight="+dayAndNight+"&startDate="+startDate+"&endDate="+endDate+"&price="+price+
				"&token=4s5s4da5d467mop423&discountPrice="+discountPrice+"&termsCon="+termsCon+"&imageName="+imageName+
				"&flightTitle="+flightTitle+"&fightStartTime="+fightStartTime+"&fightDepLoc="+fightDepLoc+"&fightDepLoc="+fightDepLoc+
				"&fightEndTime="+fightEndTime+"&flightClass="+flightClass+"&category="+category+"&intDay="+intDay+"&intTitle="+intTitle+"&intDesc="+intDesc,
				success: function(res)
				{
					var data = $.parseJSON(res);
					if(data.success == false)
					{
						$('.errorDiv').html(' <strong>Error!</strong> '+data.msg);
						$('.errorDiv').fadeIn().delay(5000).fadeOut();
						$('.errorDiv').css('color','red');

						
					}
					else if(data.success == true)
					{
						
						$('.errorDiv').html(' <strong>Success!</strong> Package Successfully Created');
						$('#package-form')[0].reset();
						$('.errorDiv').css('color','green');
						$('.errorDiv').css('display','block');
						$('.errorDiv').fadeIn().delay(7000).fadeOut();
						$('#imagePreview').html('');
						imageName = [];

					}
				},
			error: function(xhr, status, error)
			{
			}
			});

		}		


  }
  function uploadButton(){

 $("input[name = 'myfile[]']").click();
}
   var imageName = [];
   var aqAH = 1;
   var baseUrl = '<?php echo base_url(); ?>';
   $(document).ready(function ()

                    {
                      

                        $('.upload-green').remove();

                        $('.upload-progress').remove();

                        var settings = {

                            url: "<?php echo base_url(); ?>Api/uploads",

                            method: "POST",

                            allowedTypes: "pdf,jpg,jpeg,png,docx,xlsx,doc,xls",

                            fileName: "myfile",

                            multiple: true,

                            maxFileSize: 4194304,

                            showProgress: true,
                            showStatusAfterSuccess: true,
                           

                            onSuccess: function (files, data, xhr)

                            {

                                $('.upload-green').remove();

                                $('.upload-progress').remove();

                                $('#docFileNameError').html('');

                                $('#docFileName').css('border-color', 'grey');

                                var d = $.parseJSON(data);

                                imageName.push(d.image);
								console.log('imageName',imageName);
                                var fileExtension = (d.image.replace(/^.*\./, '')).toLowerCase();
                                var imgCount =0;
                                if(fileExtension == 'jpg' || fileExtension == 'jpeg' || fileExtension == 'png')
                                {
                                  if(aqAH % 2 == 1)
                                  {
                                    dymnncCls = 'slide-front';
                                  }
                                  else
                                  {
                                    dymnncCls = 'slide-back';
                                  }
                                  imgCount++;
                                  $('.gallery-item').removeClass("hide");

                                  $('#imagePreview').prepend('<div  data-image="'+d.image+'"  class="ui-state-default   kr-imagepreview aq-rfq-upload" id="' + tmp + '" ><img height="70px" width="70px" src="'+baseUrl+'uploads/' + d.image + '" /><span   onclick="removes(\'' + d.image + '\',' + tmp + ')"> <i class="fa fa-times" aria-hidden="true">x</i></span> <p>'+d.image+'</p> <b   id="' + tmp + 'a"   /> </div>');

                                  aqAH++;



                                }

                                else if(fileExtension == 'docx' || fileExtension == "doc")
                                {
                                  $('#imagePreview').prepend('<div data-image="'+d.image+'"   class="kr-imagepreview aq-rfq-upload" id="' + tmp + '" ><img height="70px" width="70px" src="'+baseUrl+'assets/img/file-icon/Word-icon.png"><span title="Remove Image" onclick="removes(\'' + d.image + '\',' + tmp + ')"> <i class="fa fa-times" aria-hidden="true">x</i></span> <p>'+d.image+'</p>   </div>');
                                }
                                else if(fileExtension == 'xlsx' ||  fileExtension == "xls")
                                {
                                  $('#imagePreview').prepend('<div data-image="'+d.image+'"   class="kr-imagepreview aq-rfq-upload" id="' + tmp + '" ><img height="70px" width="70px" src="'+baseUrl+'assets/img/file-icon/Excel-icon.png"><span title="Remove Image" onclick="removes(\'' + d.image + '\',' + tmp + ')"> <i class="fa fa-times" aria-hidden="true">x</i></span> <p>'+d.image+'</p>   </div>');
                                }
                                else if(fileExtension == 'pdf')
                                {
                                  $('#imagePreview').prepend('<div data-image="'+d.image+'"   class="kr-imagepreview aq-rfq-upload" id="' + tmp + '" ><img height="70px" width="70px" src="'+baseUrl+'assets/img/file-icon/pdf-icon-png-17.png"><span title="Remove Image" onclick="removes(\'' + d.image + '\',' + tmp + ')"> <i class="fa fa-times" aria-hidden="true">x</i></span> <p>'+d.image+'</p>  </div>');
                                }else{
									$('#imagePreview').prepend('<div data-image="'+d.image+'"   class="kr-imagepreview aq-rfq-upload" id="' + tmp + '" ><img height="70px" width="70px" src="'+baseUrl+'assets/img/file-icon/file-icon.png"><span title="Remove Image" onclick="removes(\'' + d.image + '\',' + tmp + ')"> <i class="fa fa-times" aria-hidden="true">x</i></span> <p>'+d.image+'</p>  </div>');
								}
                                tmp++;
                                  


                            },

                            afterUploadAll: function ()

                            {
                              
                              $('.upload-green').remove();

                                $('.upload-progress').remove();
                                $('#nextOne').attr('disabled',false);
                                 $('#prev').attr('disabled',false); 

                            },

                            onError: function (files, status, errMsg)

                            {

                                $("#status").html("<font color='red'>Upload is Failed</font>");

                                $('.upload-green').remove();
                                $('#nextOne').attr('disabled',false);
                                 $('#prev').attr('disabled',false); 

                            }

                        }
                        $("#mulitplefileuploader").uploadFile(settings);
                        

                    });

                    function removeElement(arr) {

                        var what, a = arguments, L = a.length, ax;

                        while (L > 1 && arr.length) {

                            what = a[--L];

                            while ((ax = arr.indexOf(what)) !== -1) {

                                arr.splice(ax, 1);

                            }

                        }

                        return arr;

                    }

                    function removes(value, id)

                    {

                        $('#' + id).remove();

                        var index = imageName.indexOf(value);

                        if (index >= 0) {

                            imageName.splice(index, 1);

                        }

                    }
				function isNumber(evt) {
					evt = (evt) ? evt : window.event;
					var charCode = (evt.which) ? evt.which : evt.keyCode;
					if (charCode > 31 && (charCode < 48 || charCode > 57)) {
						return false;
					}
					return true;
				}				
				$( function() {
					$( ".datepicker" ).datepicker();
				  } );
				  
				function removeFl(i){
					$('#fl-'+i).remove();
				}  
				function removeIn(i){
					$('#in-'+i).remove();
				}  
			
</script>