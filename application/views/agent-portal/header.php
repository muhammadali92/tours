<!DOCTYPE html>
<html>
<head>
	<title><?php echo $page_title;?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/simple-line-icons/css/simple-line-icons.css">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style-agent-portal.css">
</head>
<body>
	<?php //$this->load->view('agent-portal/account-activate');?>
	<nav class="navbar navbar-expand-sm bg-light">
		<ul class="navbar-nav">
			<a class="navbar-brand" href="<?php echo base_url();?>agent-area">Tours Logo</a>
			<li class="nav-item ">
<!-- 				<a class="nav-link" href="#">Newsfeed</a> -->
			</li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item dropdown ml-auto">
		      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
			  <?php
				echo $this->session->userdata('client_user_name');
				?>
		      </a>
		      <div class="dropdown-menu">
				<a class="dropdown-item" href="<?php echo base_url();?>">Home</a>
				<a class="dropdown-item" href="<?php echo base_url();?>agent/account_settings">Settings</a>
				<a class="dropdown-item" href="<?php echo base_url();?>Api/logout">Logout</a>
		      </div>
		    </li>
		</ul>
	</nav>
	<div class="container-fluid ">
		<div class="row">
			<div class="col-sm-3 no-padding">
				<div class="list-group">
					<a href="<?php echo base_url();?>agent-area" class="list-group-item list-group-item-action"><i class="icon icon-home"></i> Newsfeed</a>
					<a href="#" class="list-group-item list-group-item-action"><i class="icon icon-envelope"></i> Messages <span class="badge badge-pill badge-warning">3</span></a>
					<a class="list-group-item list-group-item-action" href="#"><i class="icon icon-bell"></i> Package Requests <span class="badge badge-pill badge-warning">24</span></a>
					<a href="<?php echo base_url();?>agent/manage_package" class="list-group-item list-group-item-action"><i class="icon icon-layers"></i> Package Builder</a>
					<a href="#" class="list-group-item list-group-item-action"><i class="icon icon-people"></i> Connect with Agent</a>
				</div>
			</div>

		