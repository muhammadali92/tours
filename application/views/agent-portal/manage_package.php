<?php $this->load->view('agent-portal/header');?>	
			<div class="col-sm-9">
				<div class="jumbotron">
				  <h1>Manage Package</h1>
				  <div class="container">
          <a href="<?php echo base_url(); ?>agent/package_builder">
          <button class="btn pull-left btn-success" >Add Package</button>
          </a>
          <br/><br/>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Package title</th>
        <th>Package Price</th>
        <th>Duration</th>
        <th>Package status</th>
		 <th>View</th>
        <th>Action</th>

        <th>Created date</th>

      </tr>
    </thead>
    <tbody id="package-main-table">
    </tbody>
  </table>
</div>
				   
				</div>

			</div>
<?php $this->load->view('agent-portal/footer');?>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.25.0/sweetalert2.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>

<input type="hidden" id="delId"   hidden="" >
 <div class="modal fade" id="delModal" role="dialog">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-body" style="height:150px" >
          <h2>Alert</h2>
          <div>
		  <p  >Are you sure you want to delete this item ?</p>

                        <div class="col-md-7 spaces">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success deleteItem" >Yes</button>

							</div>
              
              </div>
              <br/><br/><br/><br/><br/>
        </div>
      </div>
    </div>
  </div>  
<script>
	function getPackages(){
		$.ajax({
            url:  '<?php echo base_url(); ?>Api/getPackages',  
            type: 'GET',
            dataType: 'HTML',         
            //data : "",
            success: function(res)
            {
              let data =  $.parseJSON(res);
			  console.log('data',data);
			  
			  if (data.success == true) { 
          let html = '';
          console.log('data.data',data.data);
                if(data.data.length > 0){
                  data.data.forEach(element => {

                     let status =(element.status == 0)? 'Pending' : 'Active';
					 let url = '<?php echo base_url(); ?>package/'+element.hash;
                    html += `<tr>
                            <td>${element.package_title}</td>
                            <td>${element.price}</td>
                            <td>${element.package_days}</td>
                            <td>${status}</td>
                            <td><a target="_blank" href="${url}">view</a></td>
							<td>
							<span class="icon icon-pencil"></span>
							<span  onclick="del(${element.id})" class="icon icon-trash"></span>
							</td>
                            <td>${element.created_date}</td>
                              </tr>`;
                  });
                 $('#package-main-table').html(html);
                }
				else{
					$('#package-main-table').html('');
				}
                
                
            }
            else if (data.success == 'false') { 

              }
              else { 
				
				alert('Something went wrong');
              }
            },
            error: function(xhr, status, error)
            {
            }
            });						
	}
	getPackages();
	
	function del(id)
	{
	  $('#delId').val(id);
	  $("#delModal").modal('show');
	}
    $('.deleteItem').click(function(){
            let d = $('#delId').val();
      $.ajax({
            url: '<?php echo base_url(); ?>Api/del',  
            type: 'POST',
            dataType: 'HTML',         
            data : "id=1&deleteType=3&d="+d,
            success: function(res)
            {
              var result = $.parseJSON(res);
        if(result.success == true)
              {
        $('#delModal').modal('hide');
		swal('Good job!','Packages Successfully Deleted','success');
         getPackages();  
        }
        else
        {
			  swal('Oops...','Something went wrong! ','error');
        }
         },
            error: function(xhr, status, error)
            {
              
            }
            });     
  });
</script>