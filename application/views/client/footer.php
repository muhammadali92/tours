

<footer class="padding-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-5">
				<h3>About Tours</h3>
				<p>Tours is a company. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin augue mi, pretium a consectetur a, dictum et erat.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin augue mi, pretium a consectetur a, dictum et erat.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin augue mi, pretium a consectetur a</p>
				<a href="" class="social-icon"><img src="<?php echo base_url();?>assets/img/website-icons/social/facebook.png"></a>
				<a href="" class="social-icon"><img src="<?php echo base_url();?>assets/img/website-icons/social/twitter.png"></a>
				<a href="" class="social-icon"><img src="<?php echo base_url();?>assets/img/website-icons/social/instagram.png"></a>
			</div>
			<div class="col-sm-3">
				<h3>Contact Us</h3>
				<div class="contact-cont"><a href=""><img src="<?php echo base_url();?>assets/img/website-icons/social/phone.png"> +12-345-678</a></div>
				<div class="contact-cont"><a href=""><img src="<?php echo base_url();?>assets/img/website-icons/social/envelope.png"> info@tours.com</a></div>
				<div class="contact-cont"><a href=""><img src="<?php echo base_url();?>assets/img/website-icons/social/placeholder.png"> Central london, London UK.</a></div>
			</div>
			<div class="col-sm-4">
				<h3>Contact Form</h3>
				<form>
					<input type="text" name="" class="form-inputs" placeholder="Name">
					<input type="text" name="" class="form-inputs" placeholder="Email">
					<input type="text" name="" class="form-inputs" placeholder="Phone">
					<textarea name="" class="form-textarea" rows="6" placeholder="Message..."></textarea>
					<button type="submit" class="btn-submit">Submit</button>
				</form>
			</div>
		</div>
	</div>
</footer>



<script src="<?php echo base_url();?>assets/slick/slick.js"></script>

<script type="text/javascript">
const baseUrl = '<?php echo base_url(); ?>';
//const baseUrl = 'http://localhost/tours/';

	$('#signup-vendor-form').hide();
	$('#signup-visitor-form').hide();

	$('.banners-cont').slick({
		arrows:false,
		slidesToShow:1,
		dots:true,
		autoplay:false,
	});

	$('.cards-slider .cards-conts').slick({
		arrows:false,
		slidesToShow:3,
		dots:true,
		autoplay:false,
		slidesToScroll: 3
	});

	$('.testimonials-cont').slick({
		slidesToShow:1,
		dots:true,
		autoplay:false,
		arrows:false,
	});

	$('.blog-cont').slick({
		slidesToShow:3,
		dots:true,
	});




	$(document).ready(function(){
		$('.vendor-overlay-text').hide();

	    $(".to-vendor").click(function(){
	        $(".overlay-img").animate({
		        left: '50%',
		        'background-position-x': '-452px',
		    });

	     	$('#signup-vendor-form').hide();
	        $('.social-button-vendor-box').show();

	        $('.visitor-overlay-text').fadeOut('fast');
	        $('.vendor-overlay-text').delay(500).fadeIn('fast');
	    });

	    $(".to-visitor").click(function(){
	        $(".overlay-img").animate({
		        left: '0%',
		        'background-position-x': '-42px',
		    });

	     	$('#signup-visitor-form').hide();
	        $('.social-button-visitor-box').show();


	        $('.vendor-overlay-text').fadeOut('fast');
	        $('.visitor-overlay-text').delay(500).fadeIn('fast');
	    });


	     $('.email-btn-vendor').click(function(){
	     	$('.social-button-vendor-box').hide();
	     	$('#signup-vendor-form').fadeIn();
	     });

	     $('.email-btn-visitor').click(function(){
	     	$('.social-button-visitor-box').hide();
	     	$('#signup-visitor-form').fadeIn();
	     });

	     $('#signup-btn-main').click(function(){
	     	$('#signup-vendor-form').hide();
	        $('.social-button-vendor-box').show();
			$('#signup-visitor-form').hide();
	        $('.social-button-visitor-box').show();
	     });
	});
	function customerSignup(){
		
		let name 		= $('#cName').val();
		let email 		= $('#cEmail').val();	
		let password 	= $('#cPassword').val();	
		let phone 		= $('#cPhone').val();	
		let country 	= $('#cCountry').val();	
		let city 		= $('#cCity').val();

		$('.errorDiv').css('color','black');


		$('#cName').css('border-color','gray');
		$('#cEmail').css('border-color','gray');
		$('#cPassword').css('border-color','gray');
		$('#cPhone').css('border-color','gray');
		$('#cCountry').css('border-color','gray');
		$('#cCity').css('border-color','gray');
		
		if($.trim(name).length == 0)
		{
			$('#cName').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please enter Name.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(email).length == 0)
		{
			$('#cEmail').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please enter email.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if(validateEmail(email) == false)
		{
			$('#cEmail').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Invalid Email');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(password).length == 0)
		{
			$('#cPassword').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please enter password.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(phone).length == 0)
		{
			$('#cPhone').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please enter phone.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if(country == 0)
		{
			$('#cCountry').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please Select country.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
			$('.errorDiv').css('color','red');
		}
		else if($.trim(city).length == 0)
		{
			$('#cCity').css('border-color','red');
			$('.errorDiv').html(' <strong>Error!</strong> Please enter city.');
			$('.errorDiv').fadeIn().delay(5000).fadeOut();
			$('.errorDiv').css('color','red');

		}		 
		 else {
				$.ajax({
				url: baseUrl+'Api/customer',
				type: 'POST',
				dataType: 'HTML',
				data : "name="+name+"&email="+email+"&password="+password+"&phone="+phone+"&country="+country+"&city="+city+
				"&token=4s5s4da5d467mop423",
				success: function(res)
				{
				var data = $.parseJSON(res);
					if(data.success == false)
					{
						$('.errorDiv').html(' <strong>Error!</strong> '+data.msg);
						$('.errorDiv').fadeIn().delay(5000).fadeOut();
						$('.errorDiv').css('color','red');

						
					}
					else if(data.success == true)
					{
						
						$('.errorDiv').html(' <strong>Success!</strong> Successfully Registered');
						$('#signup-visitor-form')[0].reset();
						$('.errorDiv').css('color','green');
						$('.errorDiv').css('display','block');
						$('.errorDiv').fadeIn().delay(7000).fadeOut();

					}
				},
			error: function(xhr, status, error)
			{
			}
			});
		}		
	
	
	}
	//signup-vendor-form
	function agentSignup(){
		let companyName 		= $('#companyName').val();
		let contactPerson 	= $('#contactPerson').val();
		let email 					= $('#vendorEmail').val();	
		let password 				= $('#VendorPassword').val();	
		let phone 					= $('#vendorPhone').val();	
		let country 				= $('#vendorCountry').val();	
		let city 		  			= $('#vendorCity').val();

		$('.verrorDiv').css('color','black');


		$('#companyName').css('border-color','gray');
		$('#contactPerson').css('border-color','gray');
		$('#vendorEmail').css('border-color','gray');
		$('#VendorPassword').css('border-color','gray');
		$('#vendorPhone').css('border-color','gray');
		$('#vendorCountry').css('border-color','gray');
		$('#vendorCity').css('border-color','gray');
		
		if($.trim(companyName).length == 0)
		{
			$('#companyName').css('border-color','red');
			$('.verrorDiv').html(' <strong>Error!</strong> Please enter company Name.');
			$('.verrorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(contactPerson).length == 0)
		{
			$('#contactPerson').css('border-color','red');
			$('.verrorDiv').html(' <strong>Error!</strong> Please enter Contact Person.');
			$('.verrorDiv').fadeIn().delay(5000).fadeOut();
		}		
		
		else if($.trim(email).length == 0)
		{
			$('#vendorEmail').css('border-color','red');
			$('.verrorDiv').html(' <strong>Error!</strong> Please enter email.');
			$('.verrorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if(validateEmail(email) == false)
		{
			$('#vendorEmail').css('border-color','red');
			$('.verrorDiv').html(' <strong>Error!</strong> Invalid Email');
			$('.verrorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(password).length == 0)
		{
			$('#VendorPassword').css('border-color','red');
			$('.verrorDiv').html(' <strong>Error!</strong> Please enter password.');
			$('.verrorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if($.trim(phone).length == 0)
		{
			$('#vendorPhone').css('border-color','red');
			$('.verrorDiv').html(' <strong>Error!</strong> Please enter phone.');
			$('.verrorDiv').fadeIn().delay(5000).fadeOut();
		}		
		else if(country == 0)
		{
			$('#vendorCountry').css('border-color','red');
			$('.verrorDiv').html(' <strong>Error!</strong> Please Select country.');
			$('.verrorDiv').fadeIn().delay(5000).fadeOut();
			$('.verrorDiv').css('color','red');
		}
		else if($.trim(city).length == 0)
		{
			$('#vendorCity').css('border-color','red');
			$('.verrorDiv').html(' <strong>Error!</strong> Please enter city.');
			$('.verrorDiv').fadeIn().delay(5000).fadeOut();
			$('.verrorDiv').css('color','red');

		}		 
		 else {
				$.ajax({
				url: baseUrl+'Api/agent',
				type: 'POST',
				dataType: 'HTML',
				data : "companyName="+companyName+"&contactPerson="+contactPerson+"&email="+email+"&password="+password+"&phone="+phone+"&country="+country+"&city="+city+
				"&token=4s5s4da5d467mop423",
				success: function(res)
				{
				var data = $.parseJSON(res);
					if(data.success == false)
					{
						$('.verrorDiv').html(' <strong>Error!</strong> '+data.msg);
						$('.verrorDiv').fadeIn().delay(5000).fadeOut();
						$('.verrorDiv').css('color','red');

						
					}
					else if(data.success == true)
					{
						
						$('.verrorDiv').html(' <strong>Success!</strong> Successfully Registered');
						$('#signup-vendor-form')[0].reset();
						$('.verrorDiv').css('color','green');
						$('.verrorDiv').css('display','block');
						$('.verrorDiv').fadeIn().delay(7000).fadeOut();

					}
				},
			error: function(xhr, status, error)
			{
			}
			});
		}		
	
	
		
	}
		$("#cLoginPassword").on('keyup', function (e) {
			if (e.keyCode == 13) {
				customerLogin();
			}
		});
		  
	function customerLogin(){
		
		let email_address = $('#cLoginEmail').val();
		let password      = $('#cLoginPassword').val();
  	let reDirect = baseUrl+"home";
    
		$.ajax({
            url:  baseUrl+'Api/authentication',  
            type: 'POST',
            dataType: 'HTML',         

            data : "email_address="+email_address+"&password="+password,
            success: function(res)
            {
              if (res == '-1') { 
                 
                $('.customerLoginError').html('Email Required');
                $('.customerLoginError').css('color','red');
              
              }
              else if (res == '-2') { 
                 
                $('.customerLoginError').html('Invalid Email');
                $('.customerLoginError').css('color','red');
              
              }
              else if (res == '-3') { // if response return 1 then redirect to memberList.php
                 
                $('.customerLoginError').html('Password Required');
                $('.customerLoginError').css('color','red');
              
              }
              else if (res == '-0') { // if response return 1 then redirect to memberList.php
                 
                $('.customerLoginError').html('Invalid Email Or Password');
                $('.customerLoginError').css('color','red');
              
              }
              else if (res == '0') { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('Please Verify your account');
                $('.customerLoginError').css('color','red');
              
              }
              else if (res == '2') { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('You are blocked by Admin');
                $('.customerLoginError').css('color','red');
              
              }
              else if (res == 1) { // if response return 1 then redirect to memberList.php
                 
                $('.customerLoginError').html('');
               
                window.location = "<?php echo base_url();?>client-area";
              }
              else { 
                 
                $('.customerLoginError').html('Something Went Wrong');
                $('.customerLoginError').css('color','red');
              
              }
            },
            error: function(xhr, status, error)
            {
            }
            });		
	}
		$("#vLoginPassword").on('keyup', function (e) {
			if (e.keyCode == 13) {
				agentLogin();
			}
		});
	function agentLogin(){

		let email_address = $('#vLoginEmail').val();
		let password      = $('#vLoginPassword').val();
  	let reDirect = baseUrl+"vendor";
    
		$.ajax({
            url:  baseUrl+'Api/agentAuth',  
            type: 'POST',
            dataType: 'HTML',         

            data : "email_address="+email_address+"&password="+password,
            success: function(res)
            {
              if (res == '-1') { 
                 
                $('.agentLoginError').html('Email Required');
                $('.agentLoginError').css('color','red');
              
              }
              else if (res == '-2') { 
                 
                $('.agentLoginError').html('Invalid Email');
                $('.agentLoginError').css('color','red');
              
              }
              else if (res == '-3') { 
                 
                $('.agentLoginError').html('Password Required');
                $('.agentLoginError').css('color','red');
              
              }
              else if (res == '-0') { 
                 
                $('.agentLoginError').html('Invalid Email Or Password');
                $('.agentLoginError').css('color','red');
              
              }
              else if (res == '0') { // if response return 1 then redirect to memberList.php
                 
                $('.agentLoginError').html('Please Verify your account');
                $('.agentLoginError').css('color','red');
              
              }
              else if (res == '2') { // if response return 1 then redirect to memberList.php
                 
                $('.agentLoginError').html('You are blocked by Admin');
                $('.agentLoginError').css('color','red');
              
              }
              else if (res == 1) { // if response return 1 then redirect to memberList.php
                 
                $('.agentLoginError').html('');
               
                window.location = "<?php echo base_url();?>agent-area";
              }
              else { 
                 
                $('.agentLoginError').html('Something Went Wrong');
                $('.agentLoginError').css('color','red');
              
              }
            },
            error: function(xhr, status, error)
            {
            }
            });				
	}

	function validateEmail(email) 
	{
		var emailPattern         = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (email.search(emailPattern) == -1) // check email format
			{   
			return false;
			}
			else
			{
			return true;  
			}
	}
	$(".inp1").keyup(function(){
	  
	  $('.error_div').addClass('hide');
  });
  $(".inp1").click(function(){
	  
	  $('.error_div').addClass('hide');
  });
    function login() {


  // take input field value in variable
  var email_address = $('#email_address').val();
  var password      = $('#lpassword').val();
  var r = $('#rememberme').is(':checked');
  var reDirect ='<?php echo "/".base_url()."home"; ?>';
    $.ajax({
                                          url: '<?php echo base_url(); ?>login-auths',  
            type: 'POST',
            dataType: 'HTML',         

            data : "email_address="+email_address+"&password="+password+"&r="+r,
            success: function(res)
            {
              if (res == '-1') { 
                 
                $('#error_msg').html('Email Required');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == '-2') { 
                 
                $('#error_msg').html('Invalid Email');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == '-3') { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('Password Required');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == '-0') { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('Invalid Email Or Password');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == '0') { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('Please Verify your account');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == '2') { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('You are blocked by Admin');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == 1) { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('');
               
                window.location = "<?php echo base_url();?>client-area";
              }
              else { 
                 
                $('#error_msg').html('Something Went Wrong');
                $('.error_div').removeClass('hide');
              
              }
              
             
              
          
            },
            error: function(xhr, status, error)
            {
              //alert('Error In login');
            }
            });
    }
function forgetPassword()
  {
  	console.log('forgetPassword call');
	$('#forgetPasswordModal').modal('show');
  }

  function submitForgetPassword()
  {
  	$('#errorDiv').html('');

    $('#errorDiv').css('font-weight','normal');

    var email    =  $('#forgetEmailId').val();

    if (email.length == '')
    {
      $('#errorDiv').css('color','red');
      $('#errorDiv').css('font-weight','bold');
      $('#errorDiv').html('Please Enter Email.');
    }
    else if(validateEmail(email) == false)
      {
        $('#errorDiv').css('color','red');
      $('#errorDiv').css('font-weight','bold');
        $('#errorDiv').html('Invalid Email');
      }
    else
    {
      $.ajax({
        url:'<?php echo base_url();?>send-password-link',
        type:'POST',
        datatype:'HTML',
        data:"email="+email,
        success: function(res)
        {
          var data = $.parseJSON(res);
          if (data.errorCode == 2 && data.success == 0)
          {
            $('#errorDiv').css('color','red');
            $('#errorDiv').css('font-weight','bold');
            $('#errorDiv').html('Email not exist in our system.');
          }
          else if (data.errorCode == 4 && data.success == 0)
          {
            $('#errorDiv').css('color','red');
            $('#errorDiv').css('font-weight','bold');
            $('#errorDiv').html('Invalid Email.');
          }
          else if(data.success == 1 && data.error == 0)
          {
            $('#errorDiv').css('color','#1875bb');
            //$('#errorDiv').css('font-weight','bold');
            $('#errorDiv').html('Reset Password link has been sent. Please check your Email.');
            $('#forgetEmailId').val('');
          }
        },
      })
    }

  }	
	
	function getPackages(){
		$.ajax({
            url:  '<?php echo base_url(); ?>Api/packages',  
            type: 'GET',
            dataType: 'HTML',         
            //data : "",
            success: function(res)
            {
              let data =  $.parseJSON(res);
			  console.log('data',data);
			  
			  if (data.success == true) { 
          let html = '';
          console.log('data.data',data.data);
                if(data.data.length > 0){
                  data.data.forEach(element => {

                    let imgUrl = baseUrl+'uploads/';
										let p = '';	
										if(element.package_desc.length > 70){
											 p =  `<p class="card-text">${element.package_desc.substring(0,70)}</p>`;

										}else{
											  p =  `<p class="card-text">${element.package_desc}</p>`;

										}
										html += `				<div class="card">
														<div class="card-img-top" style="background-image:url(${imgUrl+element.images[0].image});">
															<h5>$ ${element.price}</h5>
														</div>
														<div class="card-body">
															<h5 class="card-title"><a href="${baseUrl+'package/'+element.hash}"> ${element.package_title}</a></h5>
														${p}
														</div>
														<div class="card-footer">
															<div class="card-buttons"><span class="icon icon-eye"></span> ${element.views}</div>
															<div class="card-buttons"><span class="icon icon-heart"></span> 123</div>
															<div class="card-buttons"><span class="icon icon-bubble"></span> 20</div>
															<div class="card-buttons"><span class="icon icon-share"></span></div>
														</div>
													</div>
`;
                  });
                 $('#package-main-div').html(html);
								 $(".cards-cont").not('.slick-initialized').slick({
								 		arrows:false,
										slidesToShow:3,
										dots:true,
										autoplay:false,
										slidesToScroll: 3
									});

                }
                
                
            }
            else if (data.success == 'false') { 

              }
              else { 
				
				alert('Something went wrong');
              }
            },
            error: function(xhr, status, error)
            {
            }
            });						
	}
	getPackages();
 
</script>
<script type="text/javascript">
	$('.<?php echo $active_link;?>').addClass('active-link');
</script>
</body>
</html>