<?php //$this->load->view('client/navbar');?>

	<header style="background-image: url(<?php echo base_url();?>assets/img/banner/beach2.jpg);height:450px;">
		<div class="container-fluid">
			<div class="d-flex justify-content-center text-center">
				<div class="banner-content">
					<h2>TOURS</h2>
					<p>We have various packages of tourism. Good luck with choosing.</p>
				</div>
			</div>
		</div>		
	</header>
		
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h1><b>Kerala 4 Nights - Couple Special | 4 Nights 5 Days</b></h1>
				</div>
				<div class="col-md-8">
					<div class="tour-details-slider">
					   <div class="slider" style="background-image:url(<?php echo base_url();?>assets/img/cards/bahamas.jpg);">
						</div>
					   <div class="slider" style="background-image:url(<?php echo base_url();?>assets/img/cards/dubai.jpg);">
						  </div>
					   <div class="slider" style="background-image:url(<?php echo base_url();?>assets/img/cards/paris.jpg);">
						  </div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="flight-details">
						<h3>Trip Details</h3>
						<div>
							<h6><b><i class="icon icon-location-pin"></i> STARTING FROM</b></h6>
						    <p class="no-padding">New Delhi</p>
						    <h6><b><i class="icon icon-map"></i> ITINERARY .3 CITIES</b></h6>
						    <p class="no-padding">Munnar 2N - Thekkady 1N - Allepey 1N</p>
						    <h6><b><i class="icon icon-calendar"></i> DURATION</b></h6>
							<p class="no-padding">Start Date: 19 September Wednesday</p>
							<p class="no-padding">End Date: 29 September Tuesday</p>
						</div>
						<div class="price">
							<h6>&nbsp;$3000&nbsp;</h6>
							<h4>$2750</h4>

						</div>
						<button class="btn-cust-inverse" style="color: #fff;">Book Now</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="description">
						<h3>Trip Description</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id massa sagittis, dapibus massa nec, posuere libero. In iaculis odio sed gravida dapibus. Fusce eget molestie mi, at ullamcorper ligula. Suspendisse eros sapien, tristique eget interdum in, bibendum vitae dui. Nunc dolor ante, mollis id consectetur a, convallis ac augue. Nam leo nibh, commodo sit amet hendrerit ut, commodo eu felis. In congue, sem vitae pretium sodales, erat ante tincidunt nibh, sed cursus nisl lorem et quam. Duis ac vehicula turpis. Phasellus interdum tincidunt dolor non imperdiet. Fusce efficitur facilisis elit, vitae venenatis felis sodales eget. Etiam eget mattis ante.</p><br><br>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id massa sagittis, dapibus massa nec, posuere libero. In iaculis odio sed gravida o sit amet hendrerit ut, commodo eu felis. In congue, sem vitae pretium sodales, erat ante tincidunt nibh, sed cursus nisl lorem et quam. Duis ac vehicula turpis. Phasellus interdum tincidunt dolor non imperdiet. Fusce efficitur facilisis elit, vitae venenatis felis sodales eget. Etiam eget mattis ante.</p><br>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id massa sagittis, dapibus massa nec, posuere libero. In iaculis odio sed gravida dapibus. Fusce eget molestie mi, at ullamcorper ligula. Suspendisse eros sapien, tristique eget interdum in, bibendum vitae dui. Nunc dolor ante, mollis id consectetur a, convallis ac augue. Nam leo nibh, commodo sit amet hendrerit ut, commodo eu felis. In congue, sem vitae pretium sodales, erat ante tincidunt nibh, sed cursus nisl lorem et quam. Duis ac vehicula turpis. Phasellus interdum tincidunt dolor non imperdiet. Fusce efficitur facilisis elit, vitae venenatis felis sodales eget. Etiam eget mattis ante.</p>
					</div>


					<div class="terms-conditions">
						<div class="description">
							<h3>Terms & Conditions</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id massa sagittis, dapibus massa nec, posuere libero. In iaculis odio sed gravida dapibus. </p>
							<ul>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id massa sagittis, dapibus massa nec, posuere libero. In iaculis odio sed gravida dapibus.</li>
								<li>Consectetur adipiscing elit. Proin id massa sagittis, dapibus massa nec, posuere libero. In iaculis odio sed gravida dapibus.</li>
								<li>Llor sit amet, ed gravida dapibus.</li>
								<li>Sit amet, consectetur adipiscing elit. Proin id massa sagittis, dapibus massa nec, posuere libero. In iaculis odio sed gravida dapibus.</li>
								<li>Dolor sit amet, consectetur adipiscing elit.</li>
							</ul>
						</div>
					</div>


				</div>					
				<div class="col-md-4">
					<div class="flight-details">
						<h3>Flight Details</h3>
						<div>
							<h6><b><i class="icon icon-plane"></i> FLIGHT TITLE</b></h6>
						    <p class="no-padding">Qatar Airways</p>
							<h6><b><i class="icon icon-arrow-up-circle"></i> DEPARTURE LOCATION</b></h6>
						    <p class="no-padding">New Delhi - 19 September, 8:00 AM</p>
						    <h6><b><i class="icon icon-arrow-down-circle"></i> DEPARTURE RETURN LOCATION</b></h6>
						    <p class="no-padding">Goa - 30 September, 7:30 PM</p>
						    <h6><b><i class="icon icon-badge"></i> FLIGHT CLASS</b></h6>
							<p class="no-padding">Business Class</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	

<?php $this->load->view('client/footer');?>

