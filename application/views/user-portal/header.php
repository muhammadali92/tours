<!DOCTYPE html>
<html>
<head>
	<title><?php echo $page_title;?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/simple-line-icons/css/simple-line-icons.css">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style-user-portal.css">
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-light">
		<ul class="navbar-nav">
			<a class="navbar-brand" href="<?php echo base_url(); ?>client-area">Tours Logo</a>
			<li class="nav-item ">
				<a class="nav-link" href="#">Newsfeed</a>
			</li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item dropdown ml-auto">
		      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
				<?php
				echo $this->session->userdata('client_user_name');
				?>
		      </a>
		      <div class="dropdown-menu">
				<a class="dropdown-item" href="<?php echo base_url(); ?>">Home</a>
				<a class="dropdown-item" href="#">Settings</a>
				<a class="dropdown-item" href="<?php echo base_url(); ?>Api/logout">Logout</a>
		      </div>
		    </li>
		</ul>
	</nav>
	<div class="container-fluid ">
		<div class="row">
			<div class="col-sm-3 no-padding">
				<!-- <ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#"><i class="icon icon-home"></i> Newsfeed</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#"><i class="icon icon-envelope"></i> Messages <span class="badge badge-pill badge-warning">3</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#"><i class="icon icon-calendar"></i> Itineraries</a>
					</li>					
					<li class="nav-item">
						<a class="nav-link" href="#"><i class="icon icon-layers"></i> Deals & Packages <span class="badge badge-pill badge-warning">3 New</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url();?>user/package_request"><i class="icon icon-action-redo"></i> Package Request</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#"><i class="icon icon-people"></i> Connect with Agent</a>
					</li>
				</ul> -->
				<div class="list-group">
					<a href="#" class="list-group-item list-group-item-action"><i class="icon icon-home"></i> Newsfeed</a>
					<a href="#" class="list-group-item list-group-item-action"><i class="icon icon-envelope"></i> Messages <span class="badge badge-pill badge-warning">3</span></a>
					<a href="#" class="list-group-item list-group-item-action"><i class="icon icon-calendar"></i> Itineraries</a>
					<a href="#" class="list-group-item list-group-item-action"><i class="icon icon-layers"></i> Deals & Packages <span class="badge badge-pill badge-warning">3 New</span></a>
					<a href="<?php echo base_url();?>user/package_request" class="list-group-item list-group-item-action"><i class="icon icon-action-redo"></i> Package Request</a>
					<a href="#" class="list-group-item list-group-item-action"><i class="icon icon-people"></i> Connect with Agent</a>
				</div>
			</div>
		

		


