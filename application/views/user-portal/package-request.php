<?php $this->load->view('user-portal/header');?>	
			<div class="col-sm-9">
				<div class="jumbotron">
				  <h1>Package Request</h1> 
				  <p>Build your own package.</p> 
				</div>
				<form action="#">
					<div class="row">
						<div class="col-sm-6">
							<div class="card bg-secondary mb-3">
								<div class="card-header">Mandatory Information</div>
								<div class="card-body">
									<div class="form-group">
										<label for="destination">Destination:</label>
										<select class="form-control" id="destination">
											<option>Bahamas</option>
											<option>Dubai</option>
											<option>Pakistan</option>
											<option>China</option>
										</select>
									</div>
									<div class="form-group">
										<label for="duration">Duration:</label>
										<select class="form-control" id="duration">
											<option>4-5 Days</option>
											<option>5-10 Days</option>
											<option>10-20 Days</option>
											<option>30 Days</option>
										</select>
									</div>
									<div class="form-group">
										<label for="persons">Number of persons:</label>
										<select class="form-control" id="persons">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
										</select>
									</div>
							    </div>
							</div>
							
							<div class="card bg-secondary mb-3">
								<div class="card-header">Hotel</div>
								<div class="card-body">
									<div class="form-group">
										<label for="hotel-ratings">Rating:</label>
										<select class="form-control" id="hotel-ratings">
											<option>7 Stars</option>
											<option>5 Stars</option>
											<option>3 Stars</option>
										</select>
									</div>
									<div class="form-group">
										<label>Facilities:</label>
										<div class="form-group">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="facility1" >
												<label class="custom-control-label" for="facility1">Facility 1</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="facility2" >
												<label class="custom-control-label" for="facility2">Facility 2</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="facility3" >
												<label class="custom-control-label" for="facility3">Facility 3</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="facility4" >
												<label class="custom-control-label" for="facility4">Facility 4</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label">Budget</label>
										<div class="form-group">
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<span class="input-group-text">$</span>
												</div>
												<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
												<div class="input-group-append">
													<span class="input-group-text">.00</span>
												</div>
											</div>
										</div>
									</div>
							    </div>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>

													
						<div class="col-sm-6">

							<div class="card bg-secondary mb-3">
								<div class="card-header">Flight</div>
								<div class="card-body">
									<div class="form-group">
										<label for="departure-dates">Departure dates:</label>
										<select class="form-control" id="departure-dates">
											<option>DD/MM/YYYY</option>
											<option>DD/MM/YYYY</option>
											<option>DD/MM/YYYY</option>
											<option>DD/MM/YYYY</option>
										</select>
									</div>
									<div class="form-group">
										<label for="flight-class">Class:</label>
										<select class="form-control" id="flight-class">
											<option>Business</option>
											<option>Economy</option>
										</select>
									</div>
									<div class="form-group">
										<label for="return-dates">Return dates:</label>
										<select class="form-control" id="return-dates">
											<option>DD/MM/YYYY</option>
											<option>DD/MM/YYYY</option>
											<option>DD/MM/YYYY</option>
											<option>DD/MM/YYYY</option>
										</select>
									</div>
							    </div>
							</div>

							<div class="card bg-secondary mb-3">
								<div class="card-header">Activities</div>
								<div class="card-body">									
									<div class="form-group">
										<label for="activites">Activites</label>
										<select class="form-control" id="activites">
											<option>Week days</option>
											<option>Work days</option>
											<option>All week</option>
										</select>
									</div>
									<div class="form-group">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="activity1" >
											<label class="custom-control-label" for="activity1">Beaches</label>
										</div>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="activity2" >
											<label class="custom-control-label" for="activity2">Hiking</label>
										</div>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="activity3" >
											<label class="custom-control-label" for="activity3">Off roading</label>
										</div>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="activity4" >
											<label class="custom-control-label" for="activity4">Night camps</label>
										</div>
									</div>



									<div class="form-group">
										<label class="control-label">Vehicle</label>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="car1" >
											<label class="custom-control-label" for="car1">Car</label>
										</div>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="car2" >
											<label class="custom-control-label" for="car2">SUV</label>
										</div>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="car3" >
											<label class="custom-control-label" for="car3">4x4</label>
										</div>
									</div>

									<div class="form-group">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="airport" >
											<label class="custom-control-label" for="airport">Airport pick n drop?</label>
										</div>
									</div>
							    </div>
							</div>

						</div>
					</div>
					
				</form>
			</div>
<?php $this->load->view('user-portal/footer');?>