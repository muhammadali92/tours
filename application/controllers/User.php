<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('form_validation', 'email', 'session', 'upload'));
        $this->load->model("api_manager");
		if(!$this->session->userdata('client_id'))
		{
		   header('location:'.base_url());
		  
		}
		
	}

	public function index(){
		$data['page_title'] = 'Tours | Users Dashboard';
		$this->load->view('user-portal/dashboard',$data);
	}

	public function package_request(){
		$data['page_title'] = 'Tours | Package Request';
		$this->load->view('user-portal/package-request',$data);
	}

}
?>