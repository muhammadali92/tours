<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('form_validation', 'email', 'session', 'upload'));
		$this->load->model("api_manager");


	}

	public function index(){
		$data['active_link']= 'home-a';
		$data['page_title'] = 'Tours | Home';
		$this->load->view('client/header-home.html',$data);
		$this->load->view('client/index.html',$data);
	}

	public function Tours(){
		$data['page_title'] = 'Tours | Home';
		$data['active_link']= 'tours-a';
		$this->load->view('client/navbar.html');
		$this->load->view('client/tours.html',$data);
	}

	public function Tours_details(){
		$data['page_title'] = 'Tours | Details';
		$data['active_link']= 'tours-a';
		$this->load->view('client/tours-details.html',$data);
	}

	public function Contact_us(){
		$data['page_title'] = 'Tours | Contact Us';
		$data['active_link']= 'contactus-a';
		$this->load->view('client/navbar.html',$data);
		$this->load->view('client/contact-us.html',$data);
	}

	public function Services(){
		$data['page_title'] = 'Tours | Services';
		$data['active_link']= 'services-a';
		$this->load->view('client/navbar.html',$data);
		$this->load->view('client/services.html',$data);
	}
	public function packageDetails(){
		$data['page_title'] = 'Tours | Services';
		$data['active_link']= 'services-a';
		$packageId = $this->uri->segment(2);
		$data['packageId'] = $packageId;
		
		if($packageId == ''){

			header('location:'.base_url());	
		}
		$resp = $this->api_manager->select_by_other_id('hash',$packageId,'tbl_package');

		if($resp == false){
			header('location:'.base_url());
		}

	
		$this->load->view('client/navbar.html',$data);
		$this->load->view('client/package-details.html',$data);
	}

}



?>