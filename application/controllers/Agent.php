<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library(array('form_validation', 'email', 'session', 'upload'));
        $this->load->model("api_manager");
		if(!$this->session->userdata('agent_id'))
		{
		   header('location:'.base_url());
		  
		}
		
	}

	public function index(){
		$data['page_title'] = 'Tours | Agent Dashboard';
		$this->load->view('agent-portal/dashboard',$data);
	}

	public function package_builder(){
		$data['page_title'] = 'Tours | Package Builder';
		$this->load->view('agent-portal/package_builder',$data);
	}

	public function settings(){
		$data['page_title'] = 'Tours | Settings';
		$this->load->view('agent-portal/settings.html	',$data);
	}

	public function account_settings(){
		$data['page_title'] = 'Tours | Account Settings';
		$this->load->view('agent-portal/account_settings.html',$data);
	}

	public function password_update(){
		$data['page_title'] = 'Tours | Password Update';
		$this->load->view('agent-portal/password_update',$data);
	}
	public function manage_package(){
		$data['page_title'] = 'Tours | Package Builder';
		$this->load->view('agent-portal/manage_package',$data);
	}

	public function email_verify(){
		$data['page_title'] = 'Tours | Email Verify';
		$this->load->view('agent-portal/email_verify',$data);
	}

	
}
?>