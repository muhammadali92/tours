<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Api extends CI_Controller { 
	
	function __construct()  
	{ 
		   parent::__construct();
       
		    
        $this->load->library(array('form_validation', 'email', 'session', 'upload'));
        $this->load->model("api_manager");
		$this->load->library('email');
		$this->load->helper('url');

		$this->_token = "4s5s4da5d467mop423";
	}
	public function customer() {
		if($_POST){
			
			 $token = $this->input->post('token');
			## validate Token
			if($this->validateToken($token)){
				
				$data  = array();
				$data['name'] = rawurldecode($this->input->post('name'));
				$data['email'] = rawurldecode($this->input->post('email'));
				$data['phone_number'] = rawurldecode($this->input->post('phone'));
				$data['password'] = md5(rawurldecode($this->input->post('password')));	
				$data['city'] = rawurldecode($this->input->post('city'));
				$data['countryId'] = rawurldecode($this->input->post('countryId'));
				$data['hash'] = md5(rawurldecode($this->input->post('email')));
				$data['status'] = 1;
				
				$check_email = $this->api_manager->select_by_other_id('email', $data['email'],'tbl_clients');

				if($check_email){
					$this->responseHandler(false,"Email Already Exist","");
					return false;
				}
				$emailMessage = $this->load->view('registerMail',$data,TRUE);


				$to = $data['email'];
				$subject = "Tour - Verify Your Account";
				$txt = $emailMessage;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= "From: Tours  <noreply@tours.com>" . "\r\n";
				
				## comment if localhost
				// mail($data['email'],$subject,$txt,$headers);


				$resp = $this->api_manager->insert($data,'tbl_clients');
				if($resp !== false){
					$this->responseHandler(true,"Successfully Registered ",$resp);
					return true;
					
				}
				$this->responseHandler(false,"Something went wrong","");
				return false;
				
			}
			$this->responseHandler(false,"Invalid Token","");
			return false;
		}
		$this->responseHandler(false,"Bad Request","");
		return false;
	  }
	  public function agent() {
		if($_POST){
			
			$token = $this->input->post('token');
		   ## validate Token
		   if($this->validateToken($token)){
			   
			   $data  = array();
			   $data['company_name'] = rawurldecode($this->input->post('companyName'));
			   $data['contact_person'] = rawurldecode($this->input->post('contactPerson'));
			   $data['email'] = rawurldecode($this->input->post('email'));
			   $data['phone_number'] = rawurldecode($this->input->post('phone'));
			   $data['password'] = md5(rawurldecode($this->input->post('password')));	
			   $data['city'] = rawurldecode($this->input->post('city'));
			   $data['country_id'] = rawurldecode($this->input->post('countryId'));
			   $data['hash'] = md5(rawurldecode($this->input->post('email')));
			   $data['status'] = 1;
			   
			   $check_email = $this->api_manager->select_by_other_id('email', $data['email'],'tbl_agent');

			   if($check_email){
				   $this->responseHandler(false,"Email Already Exist","");
				   return false;
			   }
			   $emailMessage = $this->load->view('registerMail',$data,TRUE);


			   $to = $data['email'];
			   $subject = "Tour - Verify Your Agent Account";
			   $txt = $emailMessage;
			   $headers  = 'MIME-Version: 1.0' . "\r\n";
			   $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			   $headers .= "From: Tours  <noreply@tours.com>" . "\r\n";
			   
			   ## comment if localhost
			   // mail($data['email'],$subject,$txt,$headers);


			   $resp = $this->api_manager->insert($data,'tbl_agent');
			   if($resp !== false){
				   $this->responseHandler(true,"Successfully Registered ",$resp);
				   return true;
				   
			   }
			   $this->responseHandler(false,"Something went wrong","");
			   return false;
			   
		   }
		   $this->responseHandler(false,"Invalid Token","");
		   return false;
	   }
	   $this->responseHandler(false,"Bad Request","");
	   return false;

	  }

	  
	public function forgetPassword() {
    if($_GET){
        $token = rawurldecode($this->input->get_post('token'));
		$email = rawurldecode($this->input->get_post('email'));
        ## validate Token
        if($this->validateToken($token)){
					if (strpos($email, "'") !== false) {
					 /// echo 'con 1';
					  $str = str_replace("'","",$email);
					//  echo $str;
					}
					else
				 {
					// echo 'con 2';
					 $str = $value;
				 }            
            $data = $this->api_model->checkExist($str,"email");
			if($data != false){
				
				$userData = $this->api_manager->getUserByEmail($email);
				
				$params['name'] = $userData[0]['name'];
				$params['password'] =  $this->randomPassword();
				$this->api_manager->updatePassword($str,$params['password']);				
				$to = $str;
				$subject = "Forgot Password";
			$emailMessage = $this->load->view('forgot',$params,TRUE);
				$txt = $emailMessage;
					
					$from = 'noreply@tours.com';
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					 
					// Create email headers
					$headers .= 'From: '.$from."\r\n".
						'Reply-To: '.$from."\r\n" .
						'X-Mailer: PHP/' . phpversion();

				if(mail($to, $subject, $txt, $headers))
				{
					$this->responseHandler(true,"success","");
					return true;					
				}				
				
				
			}
			else{
					$this->responseHandler(false,"Email not exist","");
						return false;				
			}
        }
        $this->responseHandler(false,"Invalid Token","");
        return false;
    }
    $this->responseHandler(false,"Bad Request","");
    return false;
  }
	public function randomPassword()
	{

		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}

		return implode($pass); //turn the array into a string
	}	
	

	public function userEdit() {
    if($_POST){
        
         $token = $this->input->post('token');
        ## validate Token
        if($this->validateToken($token)){
            
			$data  = array();
			$id = rawurldecode($this->input->post('userId'));
			if(isset($id))
			{
				$data['name'] = rawurldecode(addslashes($this->input->post('name')));
				$data['phone_number'] = rawurldecode($this->input->post('phone_number'));
				$data['address'] = rawurldecode($this->input->post('address'));
				$data['city'] = rawurldecode($this->input->post('city'));
				$data['countryId'] = rawurldecode($this->input->post('countryId'));

				
				$resp = $this->api_manager->update($id,$data,"tbl_clients");
				if($resp !== false){
					$this->responseHandler(true,"successfully Updated ",$resp);
					return true;
					
				}
				$this->responseHandler(false,"Something went wrong","");
				return false;
				
			}
			
        $this->responseHandler(false,"User Id required","");
        return false;
			
        }
        $this->responseHandler(false,"Invalid Token","");
        return false;
    }
    $this->responseHandler(false,"Bad Request","");
    return false;
  }
  
	public function authentication() {
    if($_POST){

			$email_address  = strip_tags($this->input->post('email_address'));
			$password		= strip_tags($this->input->post('password'));
			
			if($this->session->userdata('client_id'))
			{
			  echo 1;
			  return;
			}
			elseif(trim($email_address) == '' || trim($email_address) == null)
				{
					echo '-1';
					return;
				}
				elseif($this->emailValidate($email_address) == false)
				{
					echo '-2';
					return;
				}
				elseif(trim($password) ==  '' || trim($password) == null)
				 {
					echo '-3';
					return;
				 }
				
			
			$resp = $this->api_manager->authentication($email_address,$password);
            if($resp !== false){
				//$this->responseHandler(true,"successfully login",$resp);
				echo 1;
				return true;
				
			}
			//$this->responseHandler(false,"authentication failed",$resp);
			echo '-0';
			return false;
			
    }
    $this->responseHandler(false,"Bad Request","");
    return false;
  }

  public function getAgentDetails(){

	if(!$this->session->userdata('agent_id')){
		$this->responseHandler(false,"Invalid Request","");
		return false;

	}
	$agentId = $this->session->userdata('agent_id');
	
	$resp = $this->api_manager->select_by_id($agentId,'tbl_agent');
	if($resp !== false){
		$this->responseHandler(true,"",$resp);
		return true;
		
		}
		$this->responseHandler(false,"Something Went wrong","");
		return false;


  }

  public function agentEdit() {
    if($_POST){
        
         $token = $this->input->post('token');
        ## validate Token
        if($this->validateToken($token)){
            
			$data  = array();
			$id = rawurldecode($this->input->post('userId'));
			if(isset($id))
			{
				$data['company_name'] = rawurldecode(addslashes($this->input->post('company_name')));
				$data['contact_person'] = rawurldecode($this->input->post('contact_person'));
				$data['phone_number'] = rawurldecode($this->input->post('phone_number'));
				$data['address'] = rawurldecode($this->input->post('address'));
				$data['city'] = rawurldecode($this->input->post('city'));
				$data['country_id'] = rawurldecode($this->input->post('country_id'));
				$data['company_website'] = rawurldecode($this->input->post('company_website'));
				$data['logo'] = rawurldecode($this->input->post('logo'));
				$data['company_banner'] = rawurldecode($this->input->post('company_banner'));

				
				$resp = $this->api_manager->update($id,$data,"tbl_agent");
				if($resp !== false){
					$this->responseHandler(true,"successfully Updated ",$resp);
					return true;
					
				}
				$this->responseHandler(false,"Something went wrong","");
				return false;
				
			}
			
        $this->responseHandler(false,"User Id required","");
        return false;
			
        }
        $this->responseHandler(false,"Invalid Token","");
        return false;
    }
    $this->responseHandler(false,"Bad Request","");
    return false;
  }
  public function agentPasswordUpdate() {
    if($_POST){
        
         $token = $this->input->post('token');
        ## validate Token
        if($this->validateToken($token)){
            
			$data  = array();
			$id = rawurldecode($this->input->post('userId'));
			if(isset($id))
			{
				$data['password'] = md5(rawurldecode($this->input->post('password')));

				
				$resp = $this->api_manager->update($id,$data,"tbl_agent");
				if($resp !== false){
					$this->responseHandler(true,"successfully Updated ",$resp);
					return true;
					
				}
				$this->responseHandler(false,"Something went wrong","");
				return false;
				
			}
			
        $this->responseHandler(false,"User Id required","");
        return false;
			
        }
        $this->responseHandler(false,"Invalid Token","");
        return false;
    }
    $this->responseHandler(false,"Bad Request","");
    return false;
  }
  public function clientPasswordUpdate() {
    if($_POST){
        
         $token = $this->input->post('token');
        ## validate Token
        if($this->validateToken($token)){
            
			$data  = array();
			$id = rawurldecode($this->input->post('userId'));
			if(isset($id))
			{
				$data['password'] = md5(rawurldecode($this->input->post('password')));

				
				$resp = $this->api_manager->update($id,$data,"tbl_clients");
				if($resp !== false){
					$this->responseHandler(true,"successfully Updated ",$resp);
					return true;
					
				}
				$this->responseHandler(false,"Something went wrong","");
				return false;
				
			}
			
        $this->responseHandler(false,"User Id required","");
        return false;
			
        }
        $this->responseHandler(false,"Invalid Token","");
        return false;
    }
    $this->responseHandler(false,"Bad Request","");
    return false;
  }


  public function validateToken ($token) {
           if($token === $this->_token){
               return true;
           }
           return false;
 }
	public function responseHandler ($success,$msg,$data) {
    echo json_encode(array("success"=>$success,"msg"=>$msg,"data"=>$data));
    return true;
	}
	public function emptyNumericValidate ($field) {
		if(trim($field) == "" || $field == null || !is_numeric($field)){
			return false;
		}
		return true;
	}
	public function authenticationWithSocialNetwork() {
		if($_POST){
			$token = rawurldecode($this->input->post('token'));
			## validate Token
			if($this->validateToken($token)){
				
				$data  = array();
				$data['login_type'] = rawurldecode($this->input->post('login_type'));
				$data['social_id'] = rawurldecode($this->input->post('social_id'));
				
				$resp = $this->api_manager->authenticationWithSocialNetwork($data['email'],$data['password']);
				if($resp !== false){
					$this->responseHandler(true,"successfully login",$resp);
					return true;
					
				}
				$this->responseHandler(false,"authentication failed","");
				return false;
				
			}
			$this->responseHandler(false,"Invalid Token","");
			return false;
		}
		$this->responseHandler(false,"Bad Request","");
		return false;
	  }

	public function logout()
	  {
		  // $this->addLogs('Logout ',$this->session->userdata('avan_user_name'),$this->session->userdata('avan_id'),$this->session->userdata('avan_user_name').' has been logout');
		   $this->session->sess_destroy();
		   header('location:'.base_url());
	  }
	  
	  
	public function emailValidate($email)
		{
			  if(!filter_var($email, FILTER_VALIDATE_EMAIL))
			  {
				  return 0;
			  }
			  else
				  {
					  return 1;
				  }
		}
		public function agentAuth() {
			if($_POST){
		
					$email_address  = strip_tags($this->input->post('email_address'));
					$password		= strip_tags($this->input->post('password'));
					
					if($this->session->userdata('client_id'))
					{
					  echo 1;
					  return;
					}
					elseif(trim($email_address) == '' || trim($email_address) == null)
						{
							echo '-1';
							return;
						}
						elseif($this->emailValidate($email_address) == false)
						{
							echo '-2';
							return;
						}
						elseif(trim($password) ==  '' || trim($password) == null)
						 {
							echo '-3';
							return;
						 }
						
					
					$resp = $this->api_manager->agentAuth($email_address,$password);
					if($resp !== false){
						//$this->responseHandler(true,"successfully login",$resp);
						echo 1;
						return true;
						
					}
					//$this->responseHandler(false,"authentication failed",$resp);
					echo '-0';
					return false;
					
			}
			$this->responseHandler(false,"Bad Request","");
			return false;
		  } 

		  public function addPackage() {
			if($_POST){
				
				 $token = $this->input->post('token');
				## validate Token
				if($this->validateToken($token)){
					
					$data  = array();
					$id = $this->session->userdata('agent_id');
					if(isset($id))
					{
						$data['package_title'] = rawurldecode(addslashes($this->input->post('packageTitle')));
						$data['package_desc'] = rawurldecode($this->input->post('packageDesc'));
						$data['package_days'] = rawurldecode($this->input->post('dayAndNight'));
						$data['start_from'] = rawurldecode($this->input->post('startDate'));
						$data['end_date'] = rawurldecode($this->input->post('endDate'));
						$data['price'] = rawurldecode($this->input->post('price'));
						$data['discount_price'] = rawurldecode($this->input->post('discountPrice'));
						$data['terms_and_conditions'] = rawurldecode($this->input->post('termsCon'));
						$data['category'] = rawurldecode($this->input->post('category'));

						$data['hash'] = md5(uniqid().uniqid());
						$imageName = rawurldecode($this->input->post('imageName'));
						
						$imageNameArray = explode(",",$imageName);
						$data['vendor_id'] = $id;
						
						#### Flight Data 
						
						$flightTitle = explode(",",rawurldecode($this->input->post('flightTitle')));
						$fightStartTime = explode(",",rawurldecode($this->input->post('fightStartTime')));
						$fightDepLoc = explode(",",rawurldecode($this->input->post('fightDepLoc')));
						$fightEndTime = explode(",",rawurldecode($this->input->post('fightEndTime')));
						$flightClass = explode(",",rawurldecode($this->input->post('flightClass')));
						$flightReturnloc = explode(",",rawurldecode($this->input->post('flightReturnloc')));

						#### Itinerary Data 
						
						$intDay = explode(",",rawurldecode($this->input->post('intDay')));
						$intTitle = explode(",",rawurldecode($this->input->post('intTitle')));
						$intDesc = explode(",",rawurldecode($this->input->post('intDesc')));
						
						
		
						
						$resp = $this->api_manager->insert_get_id($data,"tbl_package");
						if($resp !== false){
							
							$isError = false;
							if(!empty($imageNameArray)){
								foreach($imageNameArray as $img){
									$imgData['image'] = $img;
									$imgData['package_id'] = $resp;
									$imgResp =  $this->api_manager->insert($imgData,"tbl_packages_img");
									if($imgResp == false){
										$isError = true;
									}									
								}
								for($i=0;$i<=count($flightTitle);$i++){
									
									$flightData['flight_name'] = $flightTitle[$i] ;
									$flightData['from'] = $fightDepLoc[$i];
									$flightData['to'] = $flightReturnloc[$i];
									$flightData['from_date'] = $fightStartTime[$i];
									$flightData['to_date'] = $fightEndTime[$i];
									$flightData['class'] = $flightClass[$i];
									$flightData['package_id'] = $resp;

									 if($flightTitle[$i] != ''){
									 $this->api_manager->insert($flightData,"tbl_flights");
									 }
									
								}
								for($i=0;$i<=count($intDay);$i++){
									
									$inData['days'] = $intDay[$i] ;
									$inData['title'] = $intTitle[$i];
									$inData['desc'] = $intDesc[$i];
									$inData['package_id'] = $resp;

									 if($flightTitle[$i] != ''){
									 $this->api_manager->insert($inData,"tbl_itinerary");
									 }
									
								}								
								
								
								if($isError == false){
									
								$this->responseHandler(true,"Successfully Create ",$resp);
								return true;
										
								}else{
									$this->responseHandler(false,"Something went wrong 1","");
									return false;									
								}									
							}
							
							
						}
						$this->responseHandler(false,"Something went wrong 2","");
						return false;
						
					}
					
				$this->responseHandler(false,"User Id required","");
				return false;
					
				}
				$this->responseHandler(false,"Invalid Token","");
				return false;
			}
			$this->responseHandler(false,"Bad Request","");
			return false;
		  }		  
			public function uploads()
			{
					
				if(isset($_FILES['myfile'])&&$_FILES['myfile']['tmp_name']!="")
				{
					$fExt = strtolower(end(explode(".",$_FILES['myfile']['name'])));
					$badExt = array('gzquar','exe','zix','jar','swf','dll','sys','scr','lnk','shs','js','wmf','com','chm','ozd','ocx','ws','scr','aru','xlm','bat','xtbl','drv','vbs','pif','bin','vbe','class','dev','xnxx','vexe','exe1','386','tps','php3','pgm','hlp','vb','vxd','buk','pcx','rsc_tmp','dxz','vba','sop','wlpginstall','boo','bkd','cla','tsa','cih','kcd','s7p','osa','exe_renamed','smm','smtmp','dom','hlw','vbx','dyz','rhk','fag','qrn','dlb','fnr','mfu','xir','lik','ctbl','dyv','bll','bxz','mjz','wsc','mjg','dli','ska','dllx','tti','fjl','upa','txs','wsh','cfxxe','xdu','uzy','bup','spam','.9','iws','oar','nls','ezt','cxq','blf','dbd','cc','xlv','rna','tko','delf','bhx','ceo','bps','atm','vzr','ce0','lkh','hsq','pid','zvz','bmw','fuj','ssy','hts','aepl','qit','mcq','dx','lok','let','plc','cyw','bqf','iva','pr','xnt','aut','lpaq5','capxml','php','hmtl','php5');
					if(!in_array($fExt,$badExt))
					{
						$config['upload_path'] = './uploads/';
						$config['allowed_types'] = '*';
						$config['max_size']	= 4096;
						$config['remove_spaces'] = true;
						$config['encrypt_name'] = true;
						$this->load->library('upload');
						$this->upload->initialize($config);
						if ($this->upload->do_upload('myfile'))
						{
							$fData = $this->upload->data();
							$data['image'] = $fData['file_name'];
							if($fExt == "jpg" || $fExt == "png" || $fExt == "jpeg")
							{
								$this->load->library('imagethumb');
								$this->imagethumb->image('./uploads/'.$data['image'],667,405,'det_');
								$this->imagethumb->image('./uploads/'.$data['image'],363,220,'li_');
								$this->imagethumb->image('./uploads/'.$data['image'],70,70,'sm_');
							}
							
							$data['rand'] = time();
							echo json_encode($data);
						
						}else{
							//echo $this->upload->display_errors();
						}
					}
				}
			}
			public function getPackages(){

				if(!$this->session->userdata('agent_id')){
					$this->responseHandler(false,"Invalid Request","");
					return false;
			
				}
				$agentId = $this->session->userdata('agent_id');
				
				$resp = $this->api_manager->select_by_other_id('vendor_id',$agentId,'tbl_package');
				if($resp !== false){
					$this->responseHandler(true,"",$resp);
					return true;
					
					}
					$this->responseHandler(false,"Something Went wrong","");
					return false;
			
			
			  }		  
			  public function packages(){

				$resp = $this->api_manager->packageDetails();
				if($resp !== false){
					$this->responseHandler(true,"",$resp);
					return true;
					
					}
					$this->responseHandler(false,"Something Went wrong","");
					return false;
			
			
			  }		  	
	public function packagesDetails(){

		
		$packageId = addSlashes(strip_tags(rawurldecode($this->input->post('packageId'))));
		$resp = $this->api_manager->packageFullDetails($packageId);

		if($resp !== false){
			$this->responseHandler(true,"",$resp);
			return true;
					
		}
			$this->responseHandler(false,"Something Went wrong","");
			return false;
			
			
	}		  
	public function del(){

		
		$id = addSlashes(strip_tags(rawurldecode($this->input->post('d'))));
		$type = addSlashes(strip_tags(rawurldecode($this->input->post('deleteType'))));
		if($type == 3){
			$table = 'tbl_package';
		}
		if($table == ''){
			$this->responseHandler(false,"Something Went wrong","");
			return false;			
		}
		$resp = $this->api_manager->delete($id,$table);

		if($resp !== false){
			$this->responseHandler(true,"",$resp);
			return true;
					
		}
			$this->responseHandler(false,"Something Went wrong","");
			return false;
			
			
	}	

}
 
