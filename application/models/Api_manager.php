<?php

class api_manager extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    ##insert function
	public function authentication($email,$password)
	{
		$sql = "SELECT id,name,email FROM tbl_clients WHERE email = '".$email."' AND password = '".md5($password)."' ";
		$query = $this->db->query($sql);
		  $row  = $query->result();
		if (count($row) > 0){
			$this->session->set_userdata(array(
				'client_id' => $row[0]->id,
				'client_user_name'=>$row[0]->name,
				'client_email_addres'=>$row[0]->email,
			
				
				));
            
            return $row;
		}
		else {
		  return false;
		}		
    }
	public function agentAuth($email,$password)
	{
		$sql = "SELECT id,contact_person,email FROM tbl_agent WHERE email = '".$email."' AND password = '".md5($password)."' ";
		$query = $this->db->query($sql);
		  $row  = $query->result();
		if (count($row) > 0){
			$this->session->set_userdata(array(
				'agent_id' => $row[0]->id,
				'client_user_name'=>$row[0]->contact_person,
				'client_email_addres'=>$row[0]->email,
			
				
				));
            
            return $row;
		}
		else {
		  return false;
		}		
    }
    
	public function authenticationWithSocialNetwork($type,$id)
	{
		$sql = "SELECT id,name,email FROM tbl_clients WHERE login_type = '".$type."' AND social_id = '".$id."' ";
		$query = $this->db->query($sql);
		  $row  = $query->result_array();
		if (count($row) > 0){
			return $row;
		}
		else {
		  return false;
		}		
    }
    
	public function getUserByEmail($email)
	{
		$sql = "SELECT id,name,email FROM tbl_clients WHERE email = ".$email." ";
		$query = $this->db->query($sql);
		  $row  = $query->result_array();
		if (count($row) > 0){
			return $row;
		}
		else {
		  return false;
		}		
	}

    public function insert($data, $table) {
        if ($this->db->insert($table, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function insert_get_id($data, $table) {
        if ($this->db->insert($table, $data)) {
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return $insert_id;
        } else {
            return false;
        }
    }

    ##update
     
    public function update($id, $data, $table) {

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }
    
    
    public function update_msg_status($resiver_id,$sender_id) {

        $this->db->trans_start();
        $this->db->where('reciever_id', $resiver_id);
        $this->db->where('sender_id', $sender_id);
        $this->db->update('app_messages',array('status' => 1));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    public function update_col_name($id, $col_name, $data, $table) {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    ##delete

    public function delete($id, $table) {
        $this->db->where('id', $id);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_by_other_id($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    ##select all 

    public function select_all($table) {

        $this->db->select("*")
                ->from($table)
                ->order_by("id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function select_all_active($table) {

        $this->db->select("*")
                ->from($table)
                ->where('is_active', 1)
                ->order_by("id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function select_all_country($table) {
        $this->db->select("*")
                ->from($table)
                ->order_by("name", "ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_all_limit($table, $offset, $limit) {

        $this->db->select("*")
                ->from($table)
                ->order_by("id", "desc")
                ->limit($limit, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    

    public function select_all_limit_search($table, $search_key, $search_text, $offset, $limit) {

        $query = $this->db->query("SELECT * from $table where $search_key LIKE '%$search_text%' LIMIT $offset,$limit");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ## acending all

    public function select_all_ASC($table) {

        $this->db->select("*")
                ->from($table)
                ->order_by("id", "ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_all_row_order($table, $row, $order) {

        $this->db->select("*")
                ->from($table)
                ->order_by($row, $order);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ##pagination count

    public function record_count($table) {
        return $this->db->count_all($table);
    }

    ##select all for pagination

    public function select_pagination($limit, $start, $table) {

        $this->db->limit($limit, $start);
        $this->db->order_by("id", "desc");
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    ## select all if status is 1

    public function select_all_status($status_col, $status, $order_by_col, $order_by, $table) {
        $this->db->where($status_col, $status)
                ->order_by($order_by_col, $order_by);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ## select by id

    public function select_by_id($id, $table) {
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }


    public function select_user_id_is_active($id, $table) {
        $this->db->where('id', $id);
        $this->db->where('is_active', 1);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function where_not_in($user_id, $col, $col_val, $table) {
        $this->db->where_not_in($col, $col_val);
        $this->db->where_not_in('user_id', $user_id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_two_id($id1, $id2, $table) {
        $this->db->where('id', $id);
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }


    ##get all for specic define foreign key id 

    public function select_by_other_id_order($col, $col_val,$order_by_key,$order, $table) {
        $this->db->where($col, $col_val);
        $this->db->order_by($order_by_key, $order);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    
    public function select_by_other_id($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_other_id_limit($col, $col_val, $table, $offset, $limit) {
        $this->db->where($col, $col_val);
        $query = $this->db->get($table, $offset, $limit);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    

    public function select_by_other_id_search_limit($col, $col_val, $search_key, $search_text, $table, $offset, $limit) {
        $this->db->where($col, $col_val);
        $this->db->like($search_key, $search_text);
        $query = $this->db->get($table, $offset, $limit);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }


    public function select_where_ids_in($ids_array, $table) {
        $query = $this->db->query('SELECT * from ' . $table . ' where id IN (' . implode(",", $ids_array) . ') ');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }


    public function get_this_by_other_id($term, $other_id, $other_value, $table) {
        $query = $this->db->query("SELECT $term FROM $table WHERE " . $other_id . " = $other_value AND is_active = 1");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

	public function getAllCountry()
	{
	   $query = $this->db->query("SELECT id,name FROM `countries`");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
	}
	
  public function userAuth($data)
  {
     // $query = $this->db->query("select id from users where email = '".$data['email']."' AND password = '".md5($data['password'])."'");
        $query = $this->db->query("select id from users where email = '".$data['email']."' AND password = '".md5($data['password'])."'  ");

        if ($query->num_rows() > 0) {

            $checkBlockQuery = $this->db->query("select id from users where email = '".$data['email']."' AND password = '".md5($data['password'])."' AND status = '1' ");
                if ($checkBlockQuery->num_rows() > 0) 
                {
                    $row = $query->result_array();
             
                    $this->session->set_userdata('user_id',$row[0]['id']);
                    return 1;
                }
                else
                {
                    return 3;
                }
            
        } 
        else
         {
            return 0;
         }

  }

  public function getDataByPk($table,$data,$id)
  {
     $query = $this->db->query("select ".$data."  FROM ".$table." WHERE id = '".$id."' ");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }
  public function getDataByFK($table,$data,$id,$fk)
  {

	 $query = $this->db->query("select ".$data."  FROM ".$table." WHERE ".$fk." = '".$id."' ");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }
  public function packageDetails()
  {
      $sql = "SELECT * FROM `tbl_package`";
      $query = $this->db->query($sql);
        $row  = $query->result_array();
      if (count($row) > 0)
      {
          foreach($row as $index => $rowData)
          {
            
            $imgSql = "SELECT * FROM `tbl_packages_img` WHERE package_id = '".$rowData["id"]."'";
            $imgquery = $this->db->query($imgSql);
            $imgArray  = $imgquery->result();
            $row[$index]['images'] = $imgArray;
          }
          return $row;
      }
      else {
        return false;
      }		
  }

  public function packageFullDetails($packageId)
  {
      $sql = "SELECT * FROM `tbl_package` where hash = '".$packageId."'";
      $query = $this->db->query($sql);
        $row  = $query->result_array();
      if (count($row) > 0)
      {
          foreach($row as $index => $rowData)
          {
            
            ### get All Images
            $imgSql = "SELECT * FROM `tbl_packages_img` WHERE package_id = '".$rowData["id"]."'";
            $imgquery = $this->db->query($imgSql);
            $imgArray  = $imgquery->result();
            $row[$index]['images'] = $imgArray;

            ### get All Flights
            $flightSql = "SELECT * FROM `tbl_flights` WHERE package_id = '".$rowData["id"]."'";
            $flightquery = $this->db->query($flightSql);
            $flightArray  = $flightquery->result();
            $row[$index]['flights'] = $flightArray;
            
        
          }
          return $row;
      }
      else {
        return false;
      }		
  }  

  
}

?>